<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KK extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('KK_model');
        $this->load->model('Penduduk_model');
    }

    public function index() {
      

         $data['title'] = 'Data Kartu Keluarga';
          $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();        
        // echo "SELAMAT DATANG ". $data ['user']['name'];

          $data['kk'] = $this->KK_model->get_all_kks();
        
          $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        //   $this->load->view('user/penduduk', $data);
        $this->load->view('user/kartu_keluarga_view', $data);
        $this->load->view('templates/footer');
    }

    public function add_kk() {
        $data = array(
            'no_kk' => $this->input->post('no_kk'),
            'kepala_keluarga' => $this->input->post('kepala_keluarga'),
            'alamat' => $this->input->post('alamat')
        );

        $this->KK_model->insert_kk($data);
        redirect('kk/index');
    }

    //fungsi untuk cek duplicate no_kk untuk JQuery

    public function check_duplicate() {
        $no_kk = $this->input->post('no_kk');
        $is_duplicate = $this->KK_model->is_duplicate_no_kk($no_kk);

        if ($is_duplicate) {
            echo 'duplicate';
        } else {
            echo 'not_duplicate';
        }
    }

    public function edit_kk($id) {
        $data['kk'] = $this->KK_model->get_kk_by_id($id);
        $this->load->view('edit_kk_modal', $data);
    }

    public function update_kk() {
        $id = $this->input->post('id');
        $data = array(
            'no_kk' => $this->input->post('no_kk'),
            'kepala_keluarga' => $this->input->post('kepala_keluarga'),
            'alamat' => $this->input->post('alamat')
        );

        $this->KK_model->update_kk($id, $data);
        redirect('kk/index');
    }

    public function delete_kk($id) {
        $this->KK_model->delete_kk($id);
        redirect('kk/index');
    }

     public function cari_data() {
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $keyword = $this->input->post('keyword');
    $data['title'] = 'Data Kartu Keluarga';

    // Pastikan $keyword diatur dan tidak kosong
    if ($keyword !== null && $keyword !== '') {
        $data['kk'] = $this->KK_model->search_kk($keyword);
    } else {
        // Handle jika keyword kosong
        $data['kk'] = $this->KK_model->get_all_kks();
    }

     $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('user/kartu_keluarga_view', $data);
        $this->load->view('templates/footer');
}

 public function print_kk(){
		 $data['kk'] = $this->KK_model->get_all_kks();
		$this->load->view('user/print_kk',$data);
	}


     public function view_anggota_kk($no_kk) {
        // Panggil model untuk mendapatkan data anggota KK
        $data['anggota_kk'] = $this->Penduduk_model->get_anggota_kk_by_no_kk($no_kk);

        // Tampilkan view untuk melihat anggota KK
        $this->load->view('kk', $data);
    }


}