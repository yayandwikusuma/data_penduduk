<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Simulasi extends CI_Controller {

    // public function __construct(){

    //     parent::__construct();
    //     $this->load->library('form_validation');

    // }

	public function halaman_angsuran()
	{
            $data['title'] = 'Simulasi Angsuran';
            $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();        
        // echo "SELAMAT DATANG ". $data ['user']['name'];

        
        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('user/angsuran',$data);
       
         $this->load->view('templates/footer');
    }

    public function kredit() {
    $totalPokok = 0;
    $totalAngsuran = 0;
    $totalJasaAngsuran = 0;

    if(isset($_POST['btnFilter'])) {
    $namaMitra = $this->input->post('namaMitra');
    $plafon = $this->input->post('plafon');
    $tglAkad = $this->input->post('tglAkad');
    $jasa = $this->input->post('jasa');
    $jangkaWaktu = $this->input->post('jangkaWaktu');
    $gracePriod = $this->input->post('gracePriod');
    $jenisKredit = $this->input->post('jenisKredit');


    // Hitung tanggal tagihan
    $tglTagihan = date('d', strtotime($tglAkad));
    $blnTagihan = date('m', strtotime($tglAkad));
    $thnTagihan = date('Y', strtotime($tglAkad));

    // echo '<table border="1">';
    // echo '<tr>
    // <th>NO</th>
    // <th>Angsuran ke-</th>
    // <th>Tanggal Tagihan</th>
    // <th>Angsuran Pokok</th>
    // <th>Jasa Angsuran</th>
    // <th>Jumlah</th>
    // </tr>';

    // hitung angsuran grace period
    // for ($j = 1; $j <= $gracePriod; $j++) {
    //     echo '<tr>';
    //     echo '<td>' . $j . '</td>';
    //     echo '<td>' . $j . '</td>';
    //     echo '<td>' . ' - ' . '</td>';
    //     echo '<td>' . number_format(0, 2, ',', '.') . '</td>';
    //     echo '<td>' . number_format(0, 2, ',', '.') . '</td>';
    //     echo '<td>' . number_format(0, 2, ',', '.') . '</td>';
    //     echo '</tr>';
    // }


    for ($i = 1; $i <= $jangkaWaktu; $i++) {
        
        //efektivitas
        if($jenisKredit==1){
        $pokok = $plafon / $jangkaWaktu;
        $jasaBln = ($plafon - (($i-1)*$pokok))* ($jasa/100) / 12;
        $angsuran = $pokok + $jasaBln;

        }
        else if($jenisKredit==2) {
            $pokok = $plafon / $jangkaWaktu;
            $jasaBln = $plafon * ($jasa/100) / 12;
            $angsuran = $pokok + $jasaBln;

        }
        else if($jenisKredit==3) {
            $jasaBln = $plafon * ($jasa/100) / 12;
            $angsuran = $pokok + $jasaBln;

        }
  
 

        if ($i == 1) {
            $tglTagihan = date('d', strtotime($tglAkad));
            $blnTagihan = date('m', strtotime($tglAkad));
            $thnTagihan = date('Y', strtotime($tglAkad));

        } else {
            $tglTagihan = date('d', strtotime('+' . ($i-1) . ' month', strtotime($tglAkad)));
            $blnTagihan = date('m', strtotime('+' . ($i-1) . ' month', strtotime($tglAkad)));
            $thnTagihan = date('Y', strtotime('+' . ($i-1) . ' month', strtotime($tglAkad)));

    }

            $data['title'] = 'Simulasi Angsuran';
            $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();        
        // echo "SELAMAT DATANG ". $data ['user']['name'];

        
        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('user/angsuran',$data);

        $this->load->view('templates/footer');

        // Tambah data angsuran ke total
    //     $totalPokok += $pokok;
    //     $totalJasaAngsuran += $jasaBln;
    //     $totalAngsuran += $angsuran;

    //     // Tampilan data angsuran
    //     echo '<tr>';
    //     echo '<td>' . ($i+$gracePriod) . '</td>';
    //     echo '<td>' . $i . '</td>';
    //     echo '<td>' . $tglTagihan . '-' . $blnTagihan . '-' . $thnTagihan . '</td>';
    //     echo '<td>' . number_format($pokok, 2, ',', '.') . '</td>';
    //     echo '<td>' . number_format($jasaBln, 2, ',', '.') . '</td>';
    //     echo '<td>' . number_format($angsuran, 2, ',', '.') . '</td>';
       
    //     echo '</tr>';
      
    // }

    //     //tabel total
    //     echo '<tr>';
    //     echo '<td colspan="3" align="left">
    //     <b>Total:</b>
    //      </td>';
    //     echo '<td><b>' . number_format($totalPokok, 2, ',', '.') . '</b></td>';
    //     echo '<td><b>' . number_format($totalJasaAngsuran, 2, ',', '.') . '</b></td>';
    //     echo '<td><b>' . number_format($totalAngsuran, 2, ',', '.') . '</b></td>';
         
    //     // echo '</td>';
    //     echo '</table>';
     }
      }
}

public function halaman_efektivitas()
	{
            $data['title'] = 'Simulasi Angsuran';
            $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();        
        // echo "SELAMAT DATANG ". $data ['user']['name'];

        
        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('user/simulasi_efektivitas',$data);
       
         $this->load->view('templates/footer');
    }
}


  
// public function kredit() {
//     $totalAngsuran = 0;
//     $totalJasaAngsuran = 0;
    
//     if(isset($_POST['btnFilter'])) {
//         $namaMitra = $this->input->post('namaMitra');
//         $plafon = $this->input->post('plafon');
//         $tglAkad = $this->input->post('tglAkad');
//         $jasa = $this->input->post('jasa');
//         $jangkaWaktu = $this->input->post('jangkaWaktu');
//         $gracePriod = $this->input->post('gracePriod');
//         $jenisKredit = $this->input->post('jenisKredit');

//         // Hitung angsuran
//         $pokok = $plafon / $jangkaWaktu;
//         $jasaBln = ($plafon * ($jasa/100)) / 12;
//         $angsuran = $pokok + $jasaBln;

//         // Hitung tanggal tagihan
//         $tglTagihan = date('d', strtotime($tglAkad));
//         $blnTagihan = date('m', strtotime($tglAkad));
//         $thnTagihan = date('Y', strtotime($tglAkad));

//         echo '<table border="1">';
//         echo '<tr><th>NO</th>
//                 <th>Angsuran ke-</th>
//                 <th>Tanggal Tagihan</th>
//                 <th>Angsuran Pokok</th>
//                 <th>Jasa Angsuran</th>
//                 <th>Jumlah</th></tr>';


//         for ($i = 1; $i <= $jangkaWaktu; $i++) {
//             // Hitung tanggal tagihan
//             if ($i == 1) {
//                 $tglTagihan = date('d', strtotime($tglAkad));
//                 $blnTagihan = date('m', strtotime($tglAkad));
//                 $thnTagihan = date('Y', strtotime($tglAkad));
//             } else {
//                 $tglTagihan = date('d', strtotime('+' . ($i-1) . ' month', strtotime($tglAkad)));
//                 $blnTagihan = date('m', strtotime('+' . ($i-1) . ' month', strtotime($tglAkad)));
//                 $thnTagihan = date('Y', strtotime('+' . ($i-1) . ' month', strtotime($tglAkad)));
//             }

//             // Tambahkan data angsuran ke total
//             $totalAngsuran += $angsuran;
//             $totalJasaAngsuran += $jasaBln;

//             // Tampilkan data angsuran
//             echo '<tr>';
//             echo '<td>' . $i . '</td>';
//             echo '<td>' . $i . '</td>';
//             echo '<td>' . $tglTagihan . '-' . $blnTagihan . '-' . $thnTagihan . '</td>';
//             echo '<td>' . number_format($pokok, 2, ',', '.') . '</td>';
//             echo '<td>' . number_format($jasaBln, 2, ',', '.') . '</td>';
//             echo '<td>' . number_format($angsuran, 2, ',', '.') . '</td>';
//             echo '</tr>';

//             $totalAngsuran = isset($totalAngsuran) ? $totalAngsuran + $angsuran : $angsuran;
//             $totalJasaAngsuran = isset($totalJasaAngsuran) ? $totalJasaAngsuran + $jasaBln : $jasaBln;
//     }
//             echo '<tr>';
//             echo '<td colspan="4" align="right"><b>Total:</b></td>';
//             echo '<td><b>' . number_format($totalJasaAngsuran, 2, ',', '.') . '</b></td>';
//             echo '<td><b>' . number_format($totalAngsuran, 2, ',', '.') . '</b></td>';
//             echo '</tr>';
//             echo '</table>';
//     }
   
// }

 
// public function kredit() {
    
//     if(isset($_POST['btnFilter'])) {
//         $namaMitra = $this->input->post('namaMitra');
//         $plafon = $this->input->post('plafon');
//         $tglAkad = $this->input->post('tglAkad');
//         $jasa = $this->input->post('jasa');
//         $jangkaWaktu = $this->input->post('jangkaWaktu');
//         $gracePriod = $this->input->post('gracePriod');
//         $jenisKredit = $this->input->post('jenisKredit');

//         // Hitung angsuran
//         $pokok = $plafon / $jangkaWaktu;
//         $jasaBln = ($plafon * ($jasa/100)) / 12;
//         $angsuran = $pokok + $jasaBln;

//         // Hitung tanggal tagihan
//         $tglTagihan = date('d', strtotime($tglAkad));
//         $blnTagihan = date('m', strtotime($tglAkad));
//         $thnTagihan = date('Y', strtotime($tglAkad));

//         // Tampilkan tabel angsuran
//         echo '<table border="1">';
//         echo '<tr><th>NO</th><th>Angsuran ke-</th><th>Tanggal Tagihan</th><th>Angsuran Pokok</th><th>Angsuran Jasa</th><th>Jumlah</th></tr>';
//         for ($i = 1; $i <= $jangkaWaktu; $i++) {
//             // Hitung tanggal tagihan
//             if ($i == 1) {
//                 $tglTagihan = date('d', strtotime($tglAkad));
//                 $blnTagihan = date('m', strtotime($tglAkad));
//                 $thnTagihan = date('Y', strtotime($tglAkad));
//             } else {
//                 $tglTagihan = date('d', strtotime('+' . ($i-1) . ' month', strtotime($tglAkad)));
//                 $blnTagihan = date('m', strtotime('+' . ($i-1) . ' month', strtotime($tglAkad)));
//                 $thnTagihan = date('Y', strtotime('+' . ($i-1) . ' month', strtotime($tglAkad)));
//             }

//             // Tampilkan data angsuran
//             echo '<tr>';
//             echo '<td>' . $i . '</td>';
//             echo '<td>' . $i . '</td>';
//             echo '<td>' . $tglTagihan . '-' . $blnTagihan . '-' . $thnTagihan . '</td>';
//             echo '<td>' . number_format($pokok, 2, ',', '.') . '</td>';
//             echo '<td>' . number_format($jasaBln, 2, ',', '.') . '</td>';
//             echo '<td>' . number_format($angsuran, 2, ',', '.') . '</td>';
//             echo '</tr>';
//         }
//         echo '</table>';
//     }
// }
