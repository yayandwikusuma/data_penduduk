<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coba extends CI_Controller {
	   public function __construct()
    {
        parent::__construct();
        $this->load->model('JudulDesa_model');
        $this->load->library('form_validation');
    }

	  
    public function index()
    {
        $data['title'] = 'Konten Header';
        $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();       
        $data['judul_desa'] = $this->JudulDesa_model->getJudulDesa();
  

         $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('user/coba', $data);
        $this->load->view('templates/footer');
    }


public function upload_avatar()
{
	$this->load->model('profile_model');
	$data['current_user'] = $this->auth_model->current_user();

	if ($this->input->method() === 'post') {
		// the user id contain dot, so we must remove it
		$file_name = str_replace('.','',$data['current_user']->id);
		$config['upload_path']          = FCPATH.'/gambar/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png';
		$config['file_name']            = $file_name;
		$config['overwrite']            = true;
		$config['max_size']             = 2048; // 1MB
		$config['max_width']            = 1080;
		$config['max_height']           = 1080;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('gambar_icon')) {
			$data['error'] = $this->upload->display_errors();
		} else {
			$uploaded_data = $this->upload->data();

			$new_data = [
				// 'id' => $data['current_user']->id,
				'avatar' => $uploaded_data['file_name'],
			];
	
			if ($this->JudulDesa_model->updateJudulDesa($new_data)) {
				$this->session->set_flashdata('message', 'Avatar updated!');
				redirect(site_url('user/coba'));
			}
		}
	}

	$this->load->view('user/coba', $data);
}
}
