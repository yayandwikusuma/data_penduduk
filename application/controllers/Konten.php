<?php
class Utama extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('penduduk_model');
         $this->load->model('JudulDesa_model');
        
        
    }

    public function index() {
        
        $data['title'] = 'Konten Judul';
        $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();        
        $data['judul_desa'] = $this->JudulDesa_model->getJudulDesa();

        
   
        

         $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('konten/juduldesa_view', $data);
        $this->load->view('templates/footer');
       
    }

   
}