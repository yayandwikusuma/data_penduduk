<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller 

{

    //  public function __construct()
    // {
    //         parent::__construct();
    //         check_login();
    //         }


    public function index(){
            $data['title'] = 'My Profile';
            $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();        
        // echo "SELAMAT DATANG ". $data ['user']['name'];

        
        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('user/profil',$data);
        $this->load->view('templates/footer');
    }
}