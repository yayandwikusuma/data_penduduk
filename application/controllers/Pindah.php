<?php
class Pindah extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Pindah_model');
        $this->load->model('Penduduk_model');
    }

    public function index()
    {
        $data['title'] = 'Data Pindah Rumah';
        $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();       
        $data['pindah'] = $this->Pindah_model->get_all_pindah();
        $data['penduduk'] = $this->Penduduk_model->dapatkan_semua_penduduk(); 
        //  $data['penduduk'] = $this->penduduk_model->dapatkan_semua_penduduk();

         $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('user/pindah_view', $data);
        $this->load->view('templates/footer');
    }



  public function tambah_pindah()
    {
        $data = array(
            'nama' => $this->input->post('nama'),
            'tanggal_pindah' => $this->input->post('tanggal_pindah'),
            'alasan' => $this->input->post('alasan'),
            'alamat_baru' => $this->input->post('alamat_baru'),
        );

        // Dapatkan no_ktp berdasarkan nama
        $no_ktp = $this->Penduduk_model->get_no_ktp_by_nama($data['nama']);

        if ($no_ktp) {
            // Jika no_ktp ditemukan, tambahkan ke data
            $data['no_ktp'] = $no_ktp;

            // Simpan data ke tabel kematian
            $this->Pindah_model->tambah_pindah($data);
            redirect('pindah/index');
        } else {
            // Handle jika nama penduduk tidak ditemukan
            echo "Nama penduduk tidak ditemukan!";
        }
    }

 

    public function edit_pindah($id)
    {

         $id = $this->input->post('id');

          $data = array(
        'nama' => $this->input->post('nama'),
            'tanggal_pindah' => $this->input->post('tanggal_pindah'),
            'alasan' => $this->input->post('alasan'),
            'alamat_baru' => $this->input->post('alamat_baru'),
    );

    $this->Pindah_model->edit_pindah($id, $data);
    redirect('pindah/index');
    }

    public function hapus_pindah($id)
    {
        $this->Pindah_model->hapus_pindah($id);
        redirect('pindah/index');
    }

    public function print_pindah(){
		 $data['pindah'] = $this->Pindah_model->get_all_pindah();
		$this->load->view('user/print_pindah',$data);
	}

    public function cari_data() {
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $keyword = $this->input->post('keyword');
    $data['title'] = 'Data Pindah Rumah';

    // Pastikan $keyword diatur dan tidak kosong
    if ($keyword !== null && $keyword !== '') {
        $data['pindah'] = $this->Kematian_model->search_pindah($keyword);
    } else {
        // Handle jika keyword kosong
        $data['pindah'] = $this->Kematian_model->get_all_pindah();
    }

     $this->load->view('templates/header',$data);
     $this->load->view('templates/sidebar',$data);
     $this->load->view('templates/topbar',$data);
     $this->load->view('user/pindah_view', $data);
     $this->load->view('templates/footer');
}

}