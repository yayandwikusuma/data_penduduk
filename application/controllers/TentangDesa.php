<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TentangDesa extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('TentangDesa_model');
    
    }

    
    public function index()
    {
        $data['title'] = 'Tentang Desa';
        $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();       
        $data['tentang_desa'] = $this->TentangDesa_model->getTentangDesa();
  

         $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('konten/aboutdesa_view', $data);
        $this->load->view('templates/footer');
    }


    
    public function edit_judul_utama($id)
    {

          $data = array(
        'judul_utama' => $this->input->post('judul_utama'),
        'deskripsi_utama' => $this->input->post('deskripsi_utama'),
    );

    $this->TentangDesa_model->edit_judul_utama($id, $data);
    redirect('TentangDesa/index');
    }

    public function edit_card_satu($id)
{
    $data = array(
        'judul_satu' => $this->input->post('judul_satu'),
        'deskripsi_satu' => $this->input->post('deskripsi_satu'),
    );

    $this->TentangDesa_model->edit_card_satu($id, $data);
    redirect('TentangDesa/index');
}

public function edit_card_dua($id)
{
    $data = array(
        'judul_dua' => $this->input->post('judul_dua'),
        'deskripsi_dua' => $this->input->post('deskripsi_dua'),
    );

    $this->TentangDesa_model->edit_card_dua($id, $data);
    redirect('TentangDesa/index');
}

public function edit_card_tiga($id)
{
    $data = array(
        'judul_tiga' => $this->input->post('judul_tiga'),
        'deskripsi_tiga' => $this->input->post('deskripsi_tiga'),
    );

    $this->TentangDesa_model->edit_card_tiga($id, $data);
    redirect('TentangDesa/index');
}

public function edit_card_empat($id)
{
    $data = array(
        'judul_empat' => $this->input->post('judul_empat'),
        'deskripsi_empat' => $this->input->post('deskripsi_empat'),
    );

    $this->TentangDesa_model->edit_card_empat($id, $data);
    redirect('TentangDesa/index');
}


}
