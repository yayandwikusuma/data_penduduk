<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Utama extends CI_Controller 

{

      public function __construct()
    {
        parent::__construct();
        $this->load->model('JudulDesa_model');
        $this->load->model('TentangDesa_model');
        $this->load->model('Penduduk_model');
        $this->load->model('KK_model');
        $this->load->model('Kematian_model');
        $this->load->model('Pindah_model');
        $this->load->model('Kelahiran_model');
        $this->load->model('Kelahiran_model');
        $this->load->model('Unggulan_model');
         $this->load->model('Kategori_model');
    }
   

    public function index(){
         $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();  
         $data['judul_desa'] = $this->JudulDesa_model->getJudulDesa();
         $data['tentang_desa'] = $this->TentangDesa_model->getTentangDesa();
         $data['jumlah_penduduk'] = $this->Penduduk_model->hitung_jumlah_penduduk();
         $data['jumlah_kk'] = $this->KK_model->hitung_jumlah_kk();
         $data['jumlah_kelahiran'] = $this->Kelahiran_model->hitung_jumlah_kelahiran();
         $data['jumlah_kematian'] = $this->Kematian_model->hitung_jumlah_kematian();
         $data['unggulan'] = $this->Unggulan_model->getUnggulan();
          $data['kategori'] = $this->Kategori_model->get_kategori();
         

   
        $this->load->view('templates/back_topbar');
        $this->load->view('frontend/utama_view',$data);
        $this->load->view('templates/back_footer');
    }

   
    
}
