<?php
class Kematian extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Kematian_model');
        $this->load->model('Penduduk_model');
    }

    public function index()
    {
        $data['title'] = 'Data Kematian';
        $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();       
        $data['kematian'] = $this->Kematian_model->get_all_kematian();
        $data['penduduk'] = $this->Penduduk_model->dapatkan_semua_penduduk(); 
        //  $data['penduduk'] = $this->penduduk_model->dapatkan_semua_penduduk();

         $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('user/kematian_view', $data);
        $this->load->view('templates/footer');
    }



  public function tambah_kematian()
    {
        $data = array(
            'nama' => $this->input->post('nama'),
            'tanggal_kematian' => $this->input->post('tanggal_kematian'),
            'sebab_kematian' => $this->input->post('sebab_kematian'),
            'alamat_pemakaman' => $this->input->post('alamat_pemakaman'),
        );

        // Dapatkan no_ktp berdasarkan nama
        $no_ktp = $this->Penduduk_model->get_no_ktp_by_nama($data['nama']);

        if ($no_ktp) {
            // Jika no_ktp ditemukan, tambahkan ke data
            $data['no_ktp'] = $no_ktp;

            // Simpan data ke tabel kematian
            $this->Kematian_model->tambah_kematian($data);
            redirect('kematian/index');
        } else {
            // Handle jika nama penduduk tidak ditemukan
            echo "Nama penduduk tidak ditemukan!";
        }
    }

 

    public function edit_kematian($id)
    {

         $id = $this->input->post('id');

          $data = array(
        'nama' => $this->input->post('nama'),
        'tanggal_kematian' => $this->input->post('tanggal_kematian'),
        'sebab_kematian' => $this->input->post('sebab_kematian'),
        'alamat_pemakaman' => $this->input->post('alamat_pemakaman'),
    );

    $this->Kematian_model->edit_kematian($id, $data);
    redirect('kematian/index');
    }

    public function hapus_kematian($id)
    {
        $this->Kematian_model->hapus_kematian($id);
        redirect('kematian/index');
    }

    public function print_kelahiran(){
		 $data['kelahiran'] = $this->Kelahiran_model->get_all_kelahiran();
		$this->load->view('user/print_kelahiran',$data);
	}

    public function cari_data() {
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $keyword = $this->input->post('keyword');
    $data['title'] = 'Data Kematian';

    // Pastikan $keyword diatur dan tidak kosong
    if ($keyword !== null && $keyword !== '') {
        $data['kematian'] = $this->Kematian_model->search_kematian($keyword);
    } else {
        // Handle jika keyword kosong
        $data['kematian'] = $this->Kematian_model->get_all_kematian();
    }

     $this->load->view('templates/header',$data);
     $this->load->view('templates/sidebar',$data);
     $this->load->view('templates/topbar',$data);
     $this->load->view('user/kematian_view', $data);
     $this->load->view('templates/footer');
}

   public function print_kematian(){
		 $data['kematian'] = $this->Kematian_model->get_all_kematian();
		$this->load->view('user/print_kematian',$data);
	}
}