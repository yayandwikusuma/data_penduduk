<?php
defined('BASEPATH') or exit('No direct script access allowed');

class JudulDesa extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('JudulDesa_model');
        $this->load->library('form_validation');
    }

    
    public function index()
    {
        $data['title'] = 'Konten Header';
        $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();       
        $data['judul_desa'] = $this->JudulDesa_model->getJudulDesa();
  

         $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('konten/juduldesa_view', $data);
        $this->load->view('templates/footer');
    }

    public function edit_judul() {
        $id = $this->input->post('id');
        $nama_desa = $this->input->post('nama_desa');
        $gambar_icon = $this->upload_gambar('gambar_icon'); // Fungsi untuk upload gambar
        $gambar_utama = $this->upload_gambar('gambar_utama');
        
        // die(); // Fungsi untuk upload gambar
        $slogan_satu = $this->input->post('slogan_satu');
        $slogan_dua = $this->input->post('slogan_dua');
            // var_dump($gambar_icon,$gambar_utama);
            // die();
        // Panggil fungsi pada model untuk melakukan update
        $this->JudulDesa_model->update_judul($id, $nama_desa, $gambar_icon, $gambar_utama, $slogan_satu, $slogan_dua);
       
        redirect('JudulDesa'); // Ganti 'halaman_tujuan' dengan halaman tujuan setelah edit
    }

    // Fungsi untuk upload gambar
    private function upload_gambar($input_name) {
        $config['upload_path'] = APPPATH.'.././gambar/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = 2048; // maksimal 2MB

        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        if ($this->upload->do_upload($input_name,$config)) {
            return $this->upload->data('file_name');
        } else {
            // Jika gagal upload, Anda bisa handle sesuai kebutuhan
             echo $this->upload->display_errors();
             $this->session->set_flashdata('error', 'Upload Gambar Icon Gagal');
            // return NULL;
        }
    }


    public function hapus_judul($id)
    {
        $this->JudulDesa_model->hapus_judul($id);
        redirect('JudulDesa');
    }
  
}