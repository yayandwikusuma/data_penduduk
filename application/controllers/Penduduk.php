<?php
class Penduduk extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('penduduk_model');
         $this->load->model('KK_model');
         $this->load->library('form_validation');
    
        
    }

    public function index() {
        
        $data['title'] = 'Data Penduduk';
        $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();        
        // echo "SELAMAT DATANG ". $data ['user']['name'];

        $data['penduduk'] = $this->penduduk_model->dapatkan_semua_penduduk();
        $data['keluarga'] = $this->KK_model->get_all_kks();


        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('user/penduduk_view', $data);
        $this->load->view('templates/footer');
       
    }


      public function searchKkSuggestions() {
            $searchTerm = $this->input->get('q');

        // Panggil model untuk melakukan pencarian saran No.KK
        $suggestions = $this->penduduk_model->searchKkSuggestions($searchTerm);

        // Encode hasil pencarian menjadi format JSON
        echo json_encode(['items' => $suggestions]);
    }

    public function cek_no_ktp() {
    $no_ktp = $this->input->post('no_ktp');

    if ($this->penduduk_model->cek_no_ktp($no_ktp)) {
        echo json_encode(array('status' => 'error', 'message' => 'Nomor KTP sudah terdaftar.'));
    } else {
        echo json_encode(array('status' => 'success', 'message' => ''));
    }
}

    

    public function proses_tambah() {
    $id_no_kk = $this->input->post('selectNoKK');
      $no_ktp = $this->input->post('no_ktp');

     if ($this->penduduk_model->cek_no_ktp($no_ktp)) {
        // Nomor KTP sudah ada, tampilkan alert
        $this->session->set_flashdata('error', 'Nomor KTP sudah terdaftar.');
    } else {
        // Ambil data kartu keluarga berdasarkan ID
        $kartu_keluarga = $this->penduduk_model->get_kartu_keluarga_by_id($id_no_kk);


    // Ambil data kartu keluarga berdasarkan ID
    // $kartu_keluarga = $this->penduduk_model->get_kartu_keluarga_by_id($id_no_kk);

        $data = array(
            'id_no_kk' => $id_no_kk,
            'no_kk' => $kartu_keluarga['no_kk'],
            'nama' => $this->input->post('nama'),
            'alamat' => $this->input->post('alamat'),
            'tanggal_lahir' => $this->input->post('tanggal_lahir'),
            'umur' => $this->input->post('umur'),
            'no_ktp' => $no_ktp,
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            'pekerjaan' => $this->input->post('pekerjaan')
        );

       // Validasi form
        $result = $this->penduduk_model->tambah_penduduk($data);

        if ($result === true) {
            // Redirect atau tampilkan pesan sukses 
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            // Pesan kesalahan
            $this->session->set_flashdata('error', $result);
        }
    }

    // Redirect ke halaman sebelumnya (modal tambah penduduk)
    redirect($_SERVER['HTTP_REFERER']);
    }



    // public function proses_tambah() {
    //     $data = array(
    //           'id_no_kk' => $this->input->post('selectNoKK'), 
    //         'no_kk' => $this->input->post('no_kk'),
    //         'nama' => $this->input->post('nama'),
    //         'alamat' => $this->input->post('alamat'),
    //         'tanggal_lahir' => $this->input->post('tanggal_lahir'),
    //         'umur' => $this->input->post('umur'),
    //         'no_ktp' => $this->input->post('no_ktp'),
    //         'jenis_kelamin' => $this->input->post('jenis_kelamin'),
    //         'pekerjaan' => $this->input->post('pekerjaan')
    //     );

    //     $this->penduduk_model->tambah_penduduk($data);
    //     redirect('penduduk/index');
    // }

    public function edit($id) {
        $data['penduduk'] = $this->penduduk_model->dapatkan_penduduk_by_id($id);
        $this->load->view('penduduk/edit', $data);
    }

    public function proses_edit($id) {
   $no_kk = $this->input->post('no_kk');


    // Ambil id_no_kk berdasarkan no_kk yang dipilih
    $id_no_kk = $this->penduduk_model->get_id_no_kk_by_no_kk($no_kk);

    $data = array(
        'id_no_kk' => $id_no_kk,
        'no_kk' => $no_kk, 
        'nama' => $this->input->post('nama'),
        'alamat' => $this->input->post('alamat'),
        'tanggal_lahir' => $this->input->post('tanggal_lahir'),
        'umur' => $this->input->post('umur'),
        'no_ktp' => $this->input->post('no_ktp'),
        'jenis_kelamin' => $this->input->post('jenis_kelamin'),
        'pekerjaan' => $this->input->post('pekerjaan')
    );



        $this->penduduk_model->edit_penduduk($id, $data);
        redirect('penduduk/index');
    }

      public function live_search() {
        $search_query = $this->input->post('query');
        $data['penduduk'] = $this->penduduk_model->live_search($search_query);
        $this->load->view('penduduk_result', $data);
    }

    // 	public function cari_data(){
	// 	$keyword = $this->input->post('keyword');
	// 	$data ['penduduk'] = $this->penduduk_model->get_keyword($keyword);
	// 	$this->load->view('templates/header');
	// 	$this->load->view('templates/sidebar');
	// 	$this->load->view('user/penduduk',$data);
	// 	$this->load->view('templates/footer');
		
	// }

   
   public function cari_data() {
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $keyword = $this->input->post('keyword');
    $data['title'] = 'Data Penduduk';

    // Pastikan $keyword diatur dan tidak kosong
    if ($keyword !== null && $keyword !== '') {
        $data['penduduk'] = $this->penduduk_model->search_penduduk($keyword);
    } else {
        // Handle jika keyword kosong
        $data['penduduk'] = $this->penduduk_model->dapatkan_semua_penduduk();
    }

     $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
          $this->load->view('user/penduduk', $data);
        $this->load->view('templates/footer');
}

    //    public function hapus($id) {
    //     $this->Penduduk_model->hapus_penduduk($id);
    //     $data['penduduk'] = $this->Penduduk_model->get_all_penduduk();
    //     $this->load->view('penduduk_result', $data);
    // }

    public function hapus($id) {
        $this->penduduk_model->hapus_penduduk($id);
        redirect('penduduk/index');
    }

    public function print_penduduk(){
		$data['penduduk'] = $this->penduduk_model->dapatkan_semua_penduduk();
		$this->load->view('user/print_penduduk',$data);
	}

    public function pdf() {
     
    $this->load->library('dompdf_gen'); 
     $this->load->model('penduduk_model'); // Pastikan model telah dimuat

    $data['penduduk'] = $this->penduduk_model->dapatkan_semua_penduduk();

    // Load view into $html variable
    $html = $this->load->view('user/laporan_pdf', $data);

    // Initialize Dompdf
  
    $this->dompdf->load_html($html);

    // (Optional) Setup paper size and orientation
    $paper_size = 'A4';
    $orientation = 'landscape'; // Perbaikan pada penulisan 'landscape'
    $html = $this->output->get_output();

    $this->dompdf->set_paper($paper_size, $orientation);

    // Render PDF (Tampilkan atau Simpan)
    $this->dompdf->render();

    // (Optional) Simpan PDF ke server
     $this->dompdf->stream('laporan_penduduk.pdf', array('Attachment' => 0));
    
    // Tampilkan PDF di browser
 //   $this->dompdf->stream('laporan_penduduk.pdf');
}

 public function view_anggota_kk($no_kk) {
        // Panggil model untuk mendapatkan data anggota KK
        $data['anggota_kk'] = $this->Penduduk_model->get_anggota_kk_by_no_kk($no_kk);

        // Tampilkan view untuk melihat anggota KK
        $this->load->view('kk', $data);
    }


}
