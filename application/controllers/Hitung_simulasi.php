<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hitung_simulasi extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        // Load model simulasi_model.php
        $this->load->model('simulasi_model');
    }

    public function halaman_angsuran()
	{
           // $data['title'] = 'Simulasi Angsuran';
            $data1['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();        
        // echo "SELAMAT DATANG ". $data ['user']['name'];

        
        $this->load->view('templates/header',$data1);
        $this->load->view('templates/sidebar',$data1);
        $this->load->view('templates/topbar',$data1);
        $this->load->view('user/simulasi_kredit',$data1);
       
         $this->load->view('templates/footer');
    }

    public function kredit()
    {
        // Mengambil data inputan dari form
        $namaMitra = $this->input->post('namaMitra');
        $plafon = $this->input->post('plafon');
        $tglAkad = $this->input->post('tglAkad');
        $jasa = $this->input->post('jasa');
        $jangkaWaktu = $this->input->post('jangkaWaktu');
        $gracePriod = $this->input->post('gracePriod');
        $jenisKredit = $this->input->post('jenisKredit');

        // Memasukkan data inputan ke dalam array
        $data = array(
            'namaMitra' => $namaMitra,
            'plafon' => $plafon,
            'tglAkad' => $tglAkad,
            'jasa' => $jasa,
            'jangkaWaktu' => $jangkaWaktu,
            'gracePriod' => $gracePriod,
            'jenisKredit' => $jenisKredit
        );

            $tglTagihan = date('d', strtotime($tglAkad));
            $blnTagihan = date('m', strtotime($tglAkad));
            $thnTagihan = date('Y', strtotime($tglAkad));

        // Memanggil method simulasi_kredit pada model dan mengirimkan data inputan
        $hasilSimulasi = $this->simulasi_model->simulasi_kredit($data);

        // Mengirimkan data hasil simulasi ke view
        $data['hasilSimulasi'] = $hasilSimulasi;

        // Memuat view simulasi_kredit.php dengan data hasil simulasi

            $data['title'] = 'Simulasi Angsuran';
            $data1['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();        
        // echo "SELAMAT DATANG ". $data ['user']['name'];

        
        $this->load->view('templates/header',$data1);
        $this->load->view('templates/sidebar',$data1);
        $this->load->view('templates/topbar',$data1);
   

        $this->load->view('user/simulasi_kredit', $data);

        $this->load->view('templates/footer');
    }

}