<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller 

{

     public function __construct()
    {
            parent::__construct();
            check_login();
            $this->load->model('Penduduk_model');
             $this->load->model('KK_model');
              $this->load->model('Kematian_model');
              $this->load->model('Pindah_model');
               $this->load->model('Kelahiran_model');
            }


    public function index(){
            $data['title'] = 'Dashboard';
            $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();        
        // echo "SELAMAT DATANG ". $data ['user']['name'];


         // Ambil data penduduk
    $data['penduduk'] = $this->Penduduk_model->dapatkan_semua_penduduk();
    $data['kk'] = $this->KK_model->get_all_kks();



    // Hitung jumlah laki-laki dan perempuan
    $data['jumlah_laki_laki'] = $this->Penduduk_model->hitung_jumlah_jenis_kelamin('Laki-Laki');
    $data['jumlah_perempuan'] = $this->Penduduk_model->hitung_jumlah_jenis_kelamin('Perempuan');
    $data['jumlah_kk'] = $this->KK_model->hitung_jumlah_kk();

    // Hitung persentase pekerjaan
    $data['persentase_pekerjaan'] = $this->Penduduk_model->hitung_persentase_pekerjaan();
    $data['jumlah_kematian'] = $this->Kematian_model->hitung_jumlah_kematian();
    $data['jumlah_pindah'] = $this->Pindah_model->hitung_jumlah_pindah();
    $data['jumlah_penduduk'] = $this->Penduduk_model->hitung_jumlah_penduduk();
    $data['jumlah_kelahiran'] = $this->Kelahiran_model->hitung_jumlah_kelahiran();

      // Mendapatkan data umur dari database
    $umur_data = $this->Penduduk_model->dapatkan_distribusi_umur();

    // Menyimpan data umur ke dalam array untuk dikirim ke view
    $data['umur_data'] = [
        'labels' => $umur_data['umur_labels'],
        'data' => $umur_data['umur_data']
    ];

    // Simpan warna untuk setiap jenis pekerjaan dalam objek
        $warna_pekerjaan = [
            'Pekerjaan1' => '#4e73df',
            'Pekerjaan2' => '#1cc88a',
            'Pekerjaan3' => '#36b9cc',
            'Pekerjaan4' => '#f6c23e',
            'Pekerjaan5' => '#e74a3b',
            'Pekerjaan6' => '#858796',
            'Pekerjaan7' => '#5a5c69',
        ];

        // Ubah objek warna ke format yang dapat digunakan oleh JavaScript
        $data['warna_pekerjaan_js'] = json_encode($warna_pekerjaan);

        $data['warna_pekerjaan'] = $warna_pekerjaan;

        
        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('user/dashboard',$data);
        $this->load->view('templates/footer');
    }
}
