<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller 

{

     public function __construct()
    {
            parent::__construct();
            check_login();
            }


    public function index(){
            $data['title'] = 'Menu Management';
            $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();        
        // echo "SELAMAT DATANG ". $data ['user']['name'];
            // ambil data menu
        $data['menu'] =  $this->db->get('user_menu')->result_array();

        $this->form_validation->set_rules('menu','Menu','required',array('required' => '<div class="alert alert-danger" role="alert">
                    Silahkan isi data menu</div>'));

        if($this -> form_validation -> run()== false){
        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('menu/index',$data);
        $this->load->view('templates/footer');

        }else{
            $this->db->insert ('user_menu', ['menu'=> $this-> input-> post ('menu')]);

            $this->session->set_flashdata('pesan','<div class="alert alert-success" role="alert">
                    Menu Baru berhasil ditambahkan!</div>');
                    redirect('menu');
        }

       
    }
    public function submenu(){
         $data['title'] = 'Sub Menu Management';
            $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();
            
            $this->load->model('Menu_model','menu');

            $data['subMenu'] =  $this->menu->getSubMenu();
            $data['menu'] = $this->db->get('user_menu')->result_array();

             $this->form_validation->set_rules('title','Title','required',array('required' => '<div class="alert alert-danger" role="alert">
                    Silahkan isi judul menu</div>'));
             $this->form_validation->set_rules('menu_id','menu','required',array('required' => '<div class="alert alert-danger" role="alert">
                    Silahkan isi sesuai menu yang diinginkan</div>'));
             $this->form_validation->set_rules('url','URL','required',array('required' => '<div class="alert alert-danger" role="alert">
                    Silahkan isi URL menu</div>'));
             $this->form_validation->set_rules('icon','icon','required',array('required' => '<div class="alert alert-danger" role="alert">
                    Silahkan isi icon menu</div>'));

        if($this->form_validation->run()== false) {

               $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('menu/submenu',$data);
        $this->load->view('templates/footer');

        }  else {

            $data = [
                'title' => $this->input->post ('title'),
                'menu_id' => $this->input->post ('menu_id'),
                'url' => $this->input->post ('url'),
                'icon' => $this->input->post ('icon'),
                'is_active' => $this->input->post ('is_active'),

            ];

            $this->db->insert('user_sub_menu',$data);
             $this->session->set_flashdata('pesan','<div class="alert alert-success" role="alert">
                    Sub Menu Baru berhasil ditambahkan!</div>');
                    redirect('menu/submenu');


        } 
     
    }
  
}
