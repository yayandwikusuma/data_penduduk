<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Gallery extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Kategori_model');
        // $this->load->model('Gambar_model');
        $this->load->library('form_validation');
    }

    
    public function index()
    {
        $data['title'] = 'Gallery Gambar';
        $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();       
        // $data['gambar'] = $this->Gambar_model->get_gambar();
        $data['kategori'] = $this->Kategori_model->get_kategori();
  

        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('konten/upload_view', $data);
        $this->load->view('templates/footer');
    }

        public function tambah_kategori() {
        $data = array(
            'nama_kategori' => $this->input->post('nama_kategori'),

        );

        $this->Kategori_model->tambah_kategori($data);
        redirect('Gallery');
    }

    //  public function edit($id) {
    //     $data['kategori'] = $this->Kategori_model->dapatkan_kategori_by_id($id);
    //     $this->load->view('Gallery/edit', $data);
    // }

    public function edit_kategori($id) {
        $data = array(
            'nama_kategori' => $this->input->post('nama_kategori'),
          
        );

     $this->Kategori_model->update_kategori($id, $data);
        redirect('Gallery');
    }


        public function hapus_kategori($id)
    {
        $this->Kategori_model->hapus_kategori($id);
        redirect('Gallery');
    }
  

    // public function edit_unggulan_satu() {
    //     $id = $this->input->post('id');
    //     $judul = $this->input->post('judul');
    //     $deskripsi = $this->input->post('deskripsi');
        
    //      // Periksa apakah ada upload gambar_satu
    // if ($_FILES['gambar_satu']['name']) {
    //     $gambar_satu = $this->upload_gambar('gambar_satu', $id);


    // } else {
    //       $gambar_satu = (!empty($this->input->post('gambar_satu_lama'))) ? $this->input->post('gambar_satu_lama') : NULL;
    //     // $gambar_satu = $this->input->post('gambar_satu_lama'); // Gunakan gambar lama jika tidak ada upload
    // }

    // // Periksa apakah ada upload gambar_dua
    // if ($_FILES['gambar_dua']['name']) {
    //     $gambar_dua = $this->upload_gambar('gambar_dua', $id);
    // } else {
    //       $gambar_dua = (!empty($this->input->post('gambar_dua_lama'))) ? $this->input->post('gambar_dua_lama') : NULL;
    //     // $gambar_dua = $this->input->post('gambar_dua_lama'); // Gunakan gambar lama jika tidak ada upload
    // }


    //     // Panggil fungsi pada model untuk melakukan update
    //     $this->Unggulan_model->update_unggulan_satu($id, $judul, $deskripsi, $gambar_satu, $gambar_dua);
       
    //     redirect('Unggulan'); // Ganti 'halaman_tujuan' dengan halaman tujuan setelah edit
    // }

    // // Fungsi untuk upload gambar
    // private function upload_gambar($input_name, $id) {
    //     $config['upload_path'] = APPPATH.'.././gambar/';
    //     $config['allowed_types'] = 'gif|jpg|jpeg|png';
    //     $config['max_size'] = 2048; // maksimal 2MB
    //     $config['overwrite'] = TRUE; // Menimpa gambar yang sudah ada dengan nama yang sama

    //     $this->load->library('upload', $config);

    //      $this->upload->initialize($config);

    //        if ($this->upload->do_upload($input_name)) {
    //     // Ambil nama file gambar yang baru diupload
    //     $new_image = $this->upload->data('file_name');

    //     // Hapus gambar yang lama jika ada
    //     $old_image = $this->Unggulan_model->get_gambar_by_id($id, $input_name);
    //     if (!empty($old_image)) {
    //         $image_path = APPPATH . '../gambar/' . $old_image;
    //         if (file_exists($image_path)) {
    //             unlink($image_path);
    //         }
    //     }

    //     return $new_image;
    // } else {
    //     // Jika gagal upload, Anda bisa handle sesuai kebutuhan
    //     echo $this->upload->display_errors();
    //     $this->session->set_flashdata('error', 'Upload Gambar ' . ucfirst($input_name) . ' Gagal');
    //     return NULL;
    // }

      
    // }


    //     public function edit_unggulan_dua() {
    //     $id = $this->input->post('id');
    //     $judul_satu = $this->input->post('judul_satu');
    //     $deskripsi_satu = $this->input->post('deskripsi_satu'); // Fungsi untuk upload gambar
    //     // $gambar_tiga = $this->upload_gambar('gambar_tiga');
    //  if ($_FILES['gambar_tiga']['name']) {
    //     $gambar_tiga = $this->upload_gambar('gambar_tiga', $id);
    // } else {
    //     $gambar_tiga = $this->input->post('gambar_tiga_lama'); // Gunakan gambar lama jika tidak ada upload
    // }

      
    //     // Panggil fungsi pada model untuk melakukan update
    //     $this->Unggulan_model->update_unggulan_dua($id, $judul_satu, $deskripsi_satu, $gambar_tiga);
       
    //     redirect('Unggulan'); // Ganti 'halaman_tujuan' dengan halaman tujuan setelah edit
    // }





}