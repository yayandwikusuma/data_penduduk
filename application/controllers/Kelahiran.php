<?php
class Kelahiran extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Kelahiran_model');
        $this->load->model('KK_model');
    }

    public function index()
    {
        $data['title'] = 'Data Kelahiran';
        $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();       
        $data['kelahiran'] = $this->Kelahiran_model->get_all_kelahiran();
        $data['keluarga'] = $this->KK_model->get_all_kks();

         $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('user/kelahiran_view', $data);
        $this->load->view('templates/footer');
    }

    public function tambah_kelahiran()
    {

    $data = array(
        'nama' => $this->input->post('nama'),
        'tanggal_lahir' => $this->input->post('tanggal_lahir'),
        'jenis_kelamin' => $this->input->post('jenis_kelamin'),
        'kepala_keluarga' => $this->input->post('kepala_keluarga'),
    );

    // Dapatkan no_kk berdasarkan kepala_keluarga
    $no_kk = $this->KK_model->get_no_kk_by_kepala_keluarga($data['kepala_keluarga']);

    if ($no_kk) {
        // Jika no_kk ditemukan, tambahkan ke data
        $data['no_kk'] = $no_kk;

        // Simpan data ke tabel kelahiran
        $this->Kelahiran_model->tambah_kelahiran($data);
        redirect('kelahiran/index');
    } else {
        // Handle jika kepala_keluarga tidak ditemukan
        echo "Kepala Keluarga tidak ditemukan!";
    }
}

 

    public function edit_kelahiran($id)
    {

         $id = $this->input->post('id');

          $data = array(
        'nama' => $this->input->post('nama'),
        'tanggal_lahir' => $this->input->post('tanggal_lahir'),
        'jenis_kelamin' => $this->input->post('jenis_kelamin'),
        'kepala_keluarga' => $this->input->post('kepala_keluarga'), // tambahkan ini
    );

    $this->Kelahiran_model->edit_kelahiran($id, $data);
    redirect('kelahiran/index');
    }

    public function hapus_kelahiran($id)
    {
        $this->Kelahiran_model->hapus_kelahiran($id);
        redirect('kelahiran/index');
    }

    public function print_kelahiran(){
		 $data['kelahiran'] = $this->Kelahiran_model->get_all_kelahiran();
		$this->load->view('user/print_kelahiran',$data);
	}

    public function cari_data() {
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $keyword = $this->input->post('keyword');
    $data['title'] = 'Data Kelahiran';

    // Pastikan $keyword diatur dan tidak kosong
    if ($keyword !== null && $keyword !== '') {
        $data['kelahiran'] = $this->Kelahiran_model->search_kelahiran($keyword);
    } else {
        // Handle jika keyword kosong
        $data['kelahiran'] = $this->Kelahiran_model->get_all_kelahiran();
    }

     $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
          $this->load->view('user/kelahiran_view', $data);
        $this->load->view('templates/footer');
}
}