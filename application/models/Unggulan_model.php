<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Unggulan_model extends CI_Model
{
     public function getUnggulan()
    {
        $query = $this->db->get('unggulan');
        return $query->result_array();
    }


      // Metode untuk mendapatkan nama gambar berdasarkan ID dan input_name
    public function get_gambar_by_id($id, $input_name) {
        $this->db->select($input_name);
        $this->db->from('unggulan'); 
        $this->db->where('id', $id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->{$input_name};
        } else {
            return NULL;
        }
    }

    public function update_unggulan_satu($id, $judul, $deskripsi, $gambar_satu, $gambar_dua) {
        $data = array(
            'judul' => $judul,
            'deskripsi' => $deskripsi,
            'gambar_satu' => $gambar_satu,
            'gambar_dua' => $gambar_dua,
          
        );

        $this->db->where('id', $id);
        $this->db->update('unggulan', $data);
    }

        public function update_unggulan_dua($id, $judul_satu, $deskripsi_satu, $gambar_tiga) {
        $data = array(
            'judul_satu' => $judul_satu,
            'deskripsi_satu' => $deskripsi_satu,
            'gambar_tiga' => $gambar_tiga,

          
        );

        $this->db->where('id', $id);
        $this->db->update('unggulan', $data);
    }



    public function hapus_unggulan($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('unggulan');
    }
}