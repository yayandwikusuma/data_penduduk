<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TentangDesa_model extends CI_Model
{
     public function getTentangDesa()
    {
        $query = $this->db->get('tentang_desa');
        return $query->result_array();
    }
     public function edit_judul_utama($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('tentang_desa', $data);
    }

      public function edit_card_satu($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('tentang_desa', $data);
    }

          public function edit_card_dua($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('tentang_desa', $data);
    }

        public function edit_card_tiga($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('tentang_desa', $data);
    }
    public function edit_card_empat($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('tentang_desa', $data);
    }


}