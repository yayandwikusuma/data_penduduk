<?php
class Penduduk_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
         
    }

   public function cek_no_ktp($no_ktp) {
    $existing_no_ktp = $this->db->get_where('penduduk', ['no_ktp' => $no_ktp])->row_array();

    return ($existing_no_ktp) ? true : false;
}  

    public function tambah_penduduk($data) {
       // Cek apakah nomor KTP sudah ada
         $existing_no_ktp = $this->cek_no_ktp($data['no_ktp']);
    //  $existing_no_ktp = $this->db->get_where('penduduk', ['no_ktp' => $data['no_ktp']])->row_array();

    if ($existing_no_ktp) {
        // Nomor KTP sudah ada, tampilkan pesan kesalahan atau lakukan aksi yang sesuai
        return 'Nomor KTP sudah terdaftar.';
    } else {
        // Nomor KTP belum ada, lanjutkan proses insert
        $this->db->insert('penduduk', $data);
        return true;
    }
    }



    public function get_kartu_keluarga_by_id($id) {
    return $this->db->get_where('kartu_keluarga', ['id' => $id])->row_array();
}




     public function searchKkSuggestions($searchTerm) {
     
        $this->db->select('no_kk, kepala_keluarga');
        $this->db->like('no_kk', $searchTerm);
        $result = $this->db->get('kartu_keluarga')->result_array();

        // Format hasil pencarian sesuai dengan kebutuhan Select2
        $suggestions = [];
        foreach ($result as $row) {
            $suggestions[] = [
                'id' => $row['no_kk'],
                'text' => $row['kepala_keluarga'],
            ];
        }

        return $suggestions;
    }
    

    public function dapatkan_semua_penduduk() {

    $this->db->select('penduduk.*, kartu_keluarga.no_kk');
    $this->db->from('penduduk');
    $this->db->join('kartu_keluarga', 'penduduk.id_no_kk = kartu_keluarga.id', 'left');
    return $this->db->get()->result_array();
    }

    public function dapatkan_penduduk_by_id($id) {
        return $this->db->get_where('penduduk', ['id' => $id])->row_array();
    }

public function get_id_no_kk_by_no_kk($no_kk) {
    $this->db->select('id');
    $this->db->where('no_kk', $no_kk);
    $result = $this->db->get('kartu_keluarga')->row_array();

    return $result['id'];
}

    public function edit_penduduk($id, $data) {
        $this->db->where('id', $id);
        return $this->db->update('penduduk', $data);
    }


      public function live_search($search_query) {
       
        $this->db->like('nama', $search_query);
        return $this->db->get('penduduk')->result_array();
    }

      public function get_keyword($keyword)
    {     
      $this->db->select('*');
      $this->db->from('penduduk');
      $this->db->like('nama',$keyword);
      $this->db->or_like('no_ktp',$keyword);
   

      return $this->db->get()->result();

    }

     public function search_penduduk($keyword) {
        $this->db->like('nama', $keyword); // Mencocokkan kata kunci dengan kolom 'nama'
        $this->db->or_like('alamat', $keyword); // Mencocokkan kata kunci dengan kolom 'alamat'
        $this->db->or_like('tanggal_lahir', $keyword); // Mencocokkan kata kunci dengan kolom 'tanggal_lahir'
        $this->db->or_like('umur', $keyword); // Mencocokkan kata kunci dengan kolom 'umur'
        $this->db->or_like('no_ktp', $keyword); // Mencocokkan kata kunci dengan kolom 'no_ktp'
        $this->db->or_like('jenis_kelamin', $keyword); // Mencocokkan kata kunci dengan kolom 'jenis_kelamin'

        $query = $this->db->get('penduduk'); // Mendapatkan data dari tabel 'penduduk'
        return $query->result_array(); // Mengembalikan hasil pencarian dalam bentuk array
    }


    public function hapus_penduduk($id) {
        // Fungsi ini akan menghapus data penduduk berdasarkan ID
        // Sesuaikan dengan struktur tabel dan kolom database Anda
        $this->db->where('id', $id);
        $this->db->delete('penduduk');
    }

     

public function get_no_ktp_by_nama($nama)
    {
        $this->db->select('no_ktp');
        $this->db->where('nama', $nama);
        $result = $this->db->get('penduduk')->row_array();

        if ($result) {
            return $result['no_ktp'];
        } else {
            return false; // Jika kepala keluarga tidak ditemukan
        }
    }


    public function hitung_jumlah_jenis_kelamin($jenis_kelamin) {
    return $this->db->where('jenis_kelamin', $jenis_kelamin)->from('penduduk')->count_all_results();
}

public function hitung_persentase_pekerjaan() {
    $total_penduduk = $this->db->count_all('penduduk');
    $query = $this->db->select('pekerjaan, COUNT(pekerjaan) as jumlah')
                      ->group_by('pekerjaan')
                      ->get('penduduk')
                      ->result_array();

    $persentase_pekerjaan = [];
    foreach ($query as $row) {
        $persentase_pekerjaan[$row['pekerjaan']] = ($row['jumlah'] / $total_penduduk) * 100;
    }

    return $persentase_pekerjaan;
    }

    public function dapatkan_distribusi_umur() {
    // Mengambil data umur dari tabel penduduk
    $this->db->select('umur, COUNT(umur) as jumlah');
    $this->db->from('penduduk');
    $this->db->group_by('umur');
    $result = $this->db->get()->result_array();

    // Menyusun data umur untuk dikirim ke controller
    $umur_labels = [];
    $umur_data = [];

    foreach ($result as $row) {
        $umur_labels[] = $row['umur'];
        $umur_data[] = $row['jumlah'];
    }

    return [
        'umur_labels' => $umur_labels,
        'umur_data' => $umur_data
    ];
}

     public function hitung_jumlah_penduduk()
    {
        return $this->db->count_all('penduduk');
    }

    public function get_anggota_kk_by_no_kk($no_kk) {
        // Ambil data anggota keluarga berdasarkan nomor KK
        $this->db->select('*');
        $this->db->from('penduduk');
        $this->db->where('no_kk', $no_kk);
        $query = $this->db->get();

        return $query->result_array();
    } 
}