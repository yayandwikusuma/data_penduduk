<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Simulasi_model extends CI_Model {

    public function simulasi_kredit($data)
    {

    $totalPokok = 0;
    $totalJasaAngsuran = 0;
    $totalSemuaAngsuran = 0;
        // Mendapatkan data inputan
        $namaMitra = $data['namaMitra'];
        $plafon = $data['plafon'];
        $tglAkad = $data['tglAkad'];
        $jasa = $data['jasa'];
        $jangkaWaktu = $data['jangkaWaktu'];
        $gracePriod = $data['gracePriod'];
        $jenisKredit = $data['jenisKredit'];

        // Proses simulasi kredit
        // ...
        // Lakukan perhitungan sesuai dengan algoritma yang Anda inginkan
        // ...

      // Misalnya, kita membuat array hasil simulasi sebagai contoh
        $hasilSimulasi = array();

        for ($i = 1; $i <= $jangkaWaktu; $i++) {
        
        //efektivitas
        if($jenisKredit==1){
        $pokokAngsuran = $plafon / $jangkaWaktu;
        $jasaAngsuran = ($plafon - (($i-1)*$pokokAngsuran))* ($jasa/100) / 12;
        $totalAngsuran = $pokokAngsuran + $jasaAngsuran;

        }
        else if($jenisKredit==2) {
            $pokokAngsuran = $plafon / $jangkaWaktu;
            $jasaAngsuran = $plafon * ($jasa/100) / 12;
            $totalAngsuran = $pokokAngsuran + $jasaAngsuran;

        }


        if ($i == 1) {
            $tglTagihan = date('d', strtotime($tglAkad));
            $blnTagihan = date('m', strtotime($tglAkad));
            $thnTagihan = date('Y', strtotime($tglAkad));

        } else {
            $tglTagihan = date('d', strtotime('+' . ($i-1) . ' month', strtotime($tglAkad)));
            $blnTagihan = date('m', strtotime('+' . ($i-1) . ' month', strtotime($tglAkad)));
            $thnTagihan = date('Y', strtotime('+' . ($i-1) . ' month', strtotime($tglAkad)));

    }

       // Tambah data angsuran ke total
        $totalPokok += $pokokAngsuran;
        $totalJasaAngsuran += $jasaAngsuran;
        $totalSemuaAngsuran += $totalAngsuran;


                // for ($i = 1; $i <= $jangkaWaktu; $i++) {
                //     $pokokAngsuran = $plafon / $jangkaWaktu;
                //     $jasaAngsuran = $plafon * ($jasa / 100) / $jangkaWaktu;
                //     $totalAngsuran = $pokokAngsuran + $jasaAngsuran;

                    $hasilSimulasi[] = array(
                        'no' => $i,
                        'angsuran_ke' => $i,
                        'jangka_waktu' => $tglTagihan . '-' . $blnTagihan . '-' . $thnTagihan ,
                        'pokok_angsuran' => number_format($pokokAngsuran, 2, ',', '.'),
                        'jasa_angsuran' => number_format($jasaAngsuran, 2, ',', '.'),
                        'total_angsuran' => number_format($totalAngsuran, 2, ',', '.'),

                        //total semua

                        'total_pokok' => number_format($totalPokok, 2, ',', '.'),
                        'total_jasa_angsuran' => number_format($totalJasaAngsuran, 2, ',', '.'),
                        'total_semua_angsuran' => number_format($totalSemuaAngsuran, 2, ',', '.'),
                        
                    );
                }

                return $hasilSimulasi;

            }
}



