<?php
defined('BASEPATH') or exit('No direct script access allowed');

class JudulDesa_model extends CI_Model
{
     public function getJudulDesa()
    {
        $query = $this->db->get('judul_desa');
        return $query->result_array();
    }

    public function update_judul($id, $nama_desa, $gambar_icon, $gambar_utama, $slogan_satu, $slogan_dua) {
        $data = array(
            'nama_desa' => $nama_desa,
            'gambar_icon' => $gambar_icon,
            'gambar_utama' => $gambar_utama,
            'slogan_satu' => $slogan_satu,
            'slogan_dua' => $slogan_dua
        );

        $this->db->where('id', $id);
        $this->db->update('judul_desa', $data);
    }

    // public function edit_judul($id, $data)
    // {
    //     $this->db->where('id', $id);
    //     $this->db->update('judul_desa', $data);
    // }

    public function tambah_judul($data)
    {
        $this->db->insert('judul_desa', $data);
    }

    public function hapus_judul($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('judul_desa');
    }
}