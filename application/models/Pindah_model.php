<?php
class Pindah_model extends CI_Model
{
       public function get_all_pindah() {
        return $this->db->get('pindah_rumah')->result_array();
    }

    public function tambah_pindah($data) {
        if ($this->cek_no_ktp_valid($data['no_ktp'])) {
            $this->db->insert('pindah_rumah', $data);
        } else {
            // Handle jika 'no_ktp' tidak valid
            echo "Nama penduduk tidak ada!";
        }
    }


     public function hitung_jumlah_pindah()
    {
        return $this->db->count_all('pindah_rumah');
    }


    public function cek_no_ktp_valid($no_ktp) {
        $this->db->where('no_ktp', $no_ktp);
        $result = $this->db->get('penduduk')->row_array();
        return !empty($result);
    }

    public function edit_pindah($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('pindah_rumah', $data);
    }

    public function get_pindah_by_id($id) {
        return $this->db->get_where('pindah_rumah', array('id' => $id))->row_array();
    }

    public function hapus_pindah($id) {
        $this->db->where('id', $id);
        $this->db->delete('pindah_rumah');
    }

    public function search_pindah($keyword) {
        $this->db->like('nama', $keyword);
        $this->db->or_like('tanggal_pindah', $keyword); 
        $query = $this->db->get('pindah_rumah'); 
        return $query->result_array(); 
    }

}