<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KK_model extends CI_Model {

    public function is_duplicate_no_kk($no_kk) {
        $this->db->where('no_kk', $no_kk);
        $query = $this->db->get('kartu_keluarga');

        return $query->num_rows() > 0;
    }

    public function insert_kk($data) {
        $this->db->insert('kartu_keluarga', $data);
    }

    public function get_all_kks() {
        return $this->db->get('kartu_keluarga')->result_array();
    }

    public function get_kk_by_id($id) {
        return $this->db->get_where('kartu_keluarga', array('id' => $id))->row_array();
    }

    public function update_kk($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('kartu_keluarga', $data);
    }


    public function delete_kk($id) {
           $this->db->where('id', $id);
        $this->db->delete('kartu_keluarga');
    
    }

       public function hitung_jumlah_kk()
    {
        return $this->db->count_all('kartu_keluarga');
    }

      public function search_kk($keyword) {
        $this->db->like('no_kk', $keyword); // Mencocokkan kata kunci dengan kolom 'nama'
        $this->db->or_like('kepala_keluarga', $keyword); // Mencocokkan kata kunci dengan kolom 'alamat'
        $this->db->or_like('alamat', $keyword); // Mencocokkan kata kunci dengan kolom 'tanggal_lahir'
     

        $query = $this->db->get('kartu_keluarga'); // Mendapatkan data dari tabel 'penduduk'
        return $query->result_array(); // Mengembalikan hasil pencarian dalam bentuk array
    }

     public function print_kk(){
		$data['kk'] = $this->penduduk_model->dapatkan_semua_penduduk();
		$this->load->view('user/print_pdf',$data);
	}

    public function cek_kepala_keluarga($kepala_keluarga)
{
    $this->db->where('kepala_keluarga', $kepala_keluarga);
    $result = $this->db->get('kartu_keluarga')->row_array();

    return !empty($result);
}

  
// public function get_no_kk_by_kepala_keluarga($kepala_keluarga)
//     {
//         $this->db->select('no_kk');
//         $this->db->where('kepala_keluarga', $kepala_keluarga);
//         $result = $this->db->get('kartu_keluarga')->row_array();

//         if ($result) {
//             return $result['no_kk'];
//         } else {
//             return false; // Jika kepala keluarga tidak ditemukan
//         }
//     }


   public function get_anggota_kk_by_no_kk($no_kk) {
        // Query untuk mengambil data anggota KK berdasarkan no_kk
        // Sesuaikan dengan struktur tabel dan kebutuhan Anda
        $query = $this->db->get_where('penduduk', array('no_kk' => $no_kk));
        return $query->result_array();
    }

}