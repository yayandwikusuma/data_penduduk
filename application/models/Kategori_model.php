<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_model extends CI_Model {


     public function get_kategori() {
        return $this->db->get('kategori')->result_array();
    }

    public function tambah_kategori($data) {
        $this->db->insert('kategori', $data);
    }
  
      public function dapatkan_kategori_by_id($id) {
        return $this->db->get_where('kategori', ['id' => $id])->row_array();
    }


    public function update_kategori($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('kategori', $data);
    }


    public function hapus_kategori($id) {
           $this->db->where('id', $id);
        $this->db->delete('kategori');
    
    }


      public function search_kategori($keyword) {

        $this->db->or_like('nama_kategori', $keyword); // Mencocokkan kata kunci dengan kolom 'tanggal_lahir'
     

        $query = $this->db->get('kategori'); // Mendapatkan data dari tabel 'penduduk'
        return $query->result_array(); // Mengembalikan hasil pencarian dalam bentuk array
    }

    

   


}