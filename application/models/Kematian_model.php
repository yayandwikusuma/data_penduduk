<?php
class Kematian_model extends CI_Model
{
       public function get_all_kematian() {
        return $this->db->get('kematian')->result_array();
    }

    public function tambah_kematian($data) {
        if ($this->cek_no_ktp_valid($data['no_ktp'])) {
            $this->db->insert('kematian', $data);
        } else {
            // Handle jika 'no_ktp' tidak valid
            echo "Nama penduduk tidak ada!";
        }
    }
 public function hitung_jumlah_kematian()
    {
        return $this->db->count_all('kematian');
    }


    public function cek_no_ktp_valid($no_ktp) {
        $this->db->where('no_ktp', $no_ktp);
        $result = $this->db->get('penduduk')->row_array();
        return !empty($result);
    }

    public function edit_kematian($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('kematian', $data);
    }

    public function get_kematian_by_id($id) {
        return $this->db->get_where('kematian', array('id' => $id))->row_array();
    }

    public function hapus_kematian($id) {
        $this->db->where('id', $id);
        $this->db->delete('kematian');
    }

    public function search_kematian($keyword) {
        $this->db->like('nama', $keyword);
        $this->db->or_like('tanggal_kematian', $keyword); 
        $query = $this->db->get('kematian'); 
        return $query->result_array(); 
    }

}