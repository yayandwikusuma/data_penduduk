<?php
class Kelahiran_model extends CI_Model
{
    public function get_all_kelahiran()
    {
        return $this->db->get('kelahiran')->result_array();
    }

    public function tambah_kelahiran($data)
    {
        if ($this->cek_no_kk_valid($data['no_kk'])) {
        $this->db->insert('kelahiran', $data);
    } else {
        // Handle jika 'no_kk' tidak valid
        echo "Nomor KK tidak valid!";
    }
    }

public function cek_no_kk_valid($no_kk)
{
    $this->db->where('no_kk', $no_kk);
    $result = $this->db->get('kartu_keluarga')->row_array();

    return !empty($result);
}

     public function hitung_jumlah_kelahiran()
    {
        return $this->db->count_all('kelahiran');
    }
  

 public function edit_kelahiran($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('kelahiran', $data);
    }

    public function get_kelahiran_by_id($id)
    {
        return $this->db->get_where('kelahiran', array('id' => $id))->row_array();
    }

    public function hapus_kelahiran($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('kelahiran');
    }

        public function get_keyword($keyword)
    {     
      $this->db->select('*');
      $this->db->from('kelahiran');
      $this->db->like('nama',$keyword);
      $this->db->or_like('tanggal_lahir',$keyword);
   

      return $this->db->get()->result();

    }


     public function search_kelahiran($keyword) {
        $this->db->like('nama', $keyword);
        $this->db->or_like('tanggal_lahir', $keyword); 
        $this->db->or_like('kepala_keluarga', $keyword); 

        $query = $this->db->get('kelahiran'); 
        return $query->result_array(); 
    }
}