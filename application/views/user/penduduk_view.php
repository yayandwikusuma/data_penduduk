
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800"><?= $title;?></h1>


                    <!-- <h2>Data Penduduk</h2> -->
                    <!-- Tombol Tambah Penduduk -->


<!-- <button type="button" class="btn btn-primary mb-3 mr-3" data-toggle="modal" data-target="#tambahModal">Tambah Penduduk</button> -->


<nav class="navbar navbar-expand-lg navbar-light bg-light">

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
      <button type="button" class="btn btn-primary mb-3 mr-2" data-toggle="modal" data-target="#tambahModal" >Tambah Penduduk</button>
      </li>
      <li class="nav-item">
      <a class= "btn btn-danger mr-2" href="<?php echo base_url('penduduk/print_penduduk')?>"><i class="fa fa-print"></i> Print</a>
      </li>
       <li class="nav-item">
      <a class= "btn btn-warning" href="<?php echo base_url('penduduk/pdf')?>"><i class="fa fa-file"></i> Export PDF</a>
      </li>



    </ul>
    <div class="form-inline my-2 my-lg-2">
    <?php echo form_open('penduduk/cari_data')?>
      <input type="text" class="form-control mr-sm-2"  placeholder="Search" aria-label="Search" name= "keyword">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Cari</button>
      <?php echo form_close()?>
    </div>
  </div>
</nav>

                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                             <div class="d-flex justify-content-between align-items-center">
                            <h6 class="m-0 font-weight-bold text-primary mb-3"  style="font-size: 20px;">Penduduk </h6>
                                                  
                        </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered " id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                        <th style="width: 5%;">No.</th>
                                            <th style="width: 15%; text-align: center;" >Nama </th>
                                            <th style="width: 20%;text-align: center;">Alamat </th>
                                            <th style="width: 12%; text-align: center;" >Tgl Lahir </th>
                                            <th style="width: 7%; text-align: center;" >Umur</th>
                                            <th style="width: 10%; text-align: center;" >No.KTP</th>
                                             <th style="width: 10%; text-align: center;" >No.KK</th>
                                            <th style="width: 5%; text-align: center;" >Jenis Kelamin</th>
                                            <th style="width: 10%; text-align: center;" >Pekerjaan</th>
                                             <th style="width: 13%; text-align: center;" >Aksi</th>


                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; ?>
                                          <?php foreach ($penduduk as $item) : ?>
                                    <tr>
                                        <th scope="row"><?= $i; ?></th>
                                        <td style= "text-align: left"><?= $item['nama']; ?></td>
                                        <td style= "text-align: left"><?= $item['alamat']; ?></td>
                                        <td style= "text-align: justify"><?= date('d-m-Y', strtotime($item['tanggal_lahir'])); ?></td>
                                        <td style= "text-align: justify"><?= $item['umur'];  ?> thn</td>
                                        <td style= "text-align: justify"><?= $item['no_ktp']; ?></td>
                                         <td style= "text-align: justify"><?= $item['no_kk']; ?></td>
                                        <td style= "text-align: justify"><?= $item['jenis_kelamin']; ?></td>
                                        <td style= "text-align: justify"><?= $item['pekerjaan']; ?></td>
                                        <td>
                                        <a href="#" class="badge badge-primary" data-toggle="modal" data-target="#editModal<?= $item['id']; ?>">Edit</a>
                                        <a href="<?= base_url('penduduk/hapus/' . $item['id']); ?>" class="badge badge-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Hapus</a>
                                        </td>
                               
                                    </tr>
                                    <?php $i++; ?>






            <!-- Modal Edit Penduduk -->
            <div class="modal fade" id="editModal<?=$item['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="editModalLabel<?=$item['id']; ?>" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="editModalLabel<?=$item['id']; ?>">Edit Penduduk</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <!-- Formulir Edit Penduduk -->
                            <form action="<?= base_url('penduduk/proses_edit/' .$item['id']); ?>" method="post">
                                             <div class="form-group">
                        <label for="kepala_keluarga">No.KK :</label>
                        <select name="no_kk" id="editNoKK<?= $item['id']; ?>" class="form-control" >
                            <?php foreach ($keluarga as $kk) : ?>
                                 <!-- <?php $optionValue = $kk['no_kk'] . '(' . $kk['kepala_keluarga'] . ')'; ?> -->
                                <option value="<?= $kk['no_kk']; ?>" <?= ($item['no_kk'] == $kk['no_kk']) ? 'selected' : ''; ?>><?= $kk['kepala_keluarga']  . ' (' . $kk['no_kk'] . ')'; ?></option>
                                  <!-- <?= $optionValue; ?> -->
                                <!-- </option> -->
                            <?php endforeach; ?>
                        </select>
                    </div>
                            <div class="form-group">
                                <label for="nama">Nama:</label>
                                <input type="text" name="nama" value="<?=$item['nama']; ?>" required class="form-control">
                            </div>
                            <div class="form-group">

                                <label for="alamat">Alamat:</label>
                                <textarea name="alamat" required class="form-control"><?=$item['alamat']; ?></textarea>
                                 </div>
                                
                                <div class="form-row">
                                     <div class="form-group col-md-6">
                                    <label for="tanggal_lahir">Tanggal Lahir:</label>
                                    <input type="date" name="tanggal_lahir" placeholder="dd mm yy" value="<?=$item['tanggal_lahir']; ?>" required class="form-control">
                                  </div>

                                <div div class="form-group col-md-6">
                                <label for="umur">Umur:</label>
                                <input type="number" name="umur" value="<?=$item['umur']; ?>" required class="form-control">
                                   </div>
                                 </div>


                                <div class="form-group">

                                <label for="no_ktp">Nomor KTP:</label>
                                <input type="text" name="no_ktp" value="<?=$item['no_ktp']; ?>" required class="form-control">
                                </div>
                                <div class="form-group">

                                <label for="jenis_kelamin">Jenis Kelamin:</label>
                                <select name="jenis_kelamin" required class="form-control">
                                    <option value="Laki-laki" <?= ($item['jenis_kelamin'] == 'Laki-laki') ? 'selected' : ''; ?>>Laki-laki</option>
                                    <option value="Perempuan" <?= ($item['jenis_kelamin'] == 'Perempuan') ? 'selected' : ''; ?>>Perempuan</option>
                                </select>
                                </div>
                                <div class="form-group">
                                <label for="alamat">Pekerjaan:</label>
                                <input type="text" name="pekerjaan" value="<?=$item['pekerjaan']; ?>" required class="form-control">
                                 </div>

                                <div class="form-group text-right">
                                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                <input type="submit" class="btn btn-primary" value="Simpan">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </tbody>
</table>

<!-- Tombol Tambah Penduduk -->



<!-- Modal Tambah Penduduk -->
<div class="modal fade" id="tambahModal" tabindex="-1" role="dialog" aria-labelledby="tambahModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="tambahModalLabel">Tambah Penduduk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Formulir Tambah Penduduk -->
                <form action="<?= base_url('penduduk/proses_tambah'); ?>" method="post">
                <div class="form-group">
                    <label for="no_kk">No.KK :</label>
                    <select class="form-control select2" style="width: 100%" name="selectNoKK" id="selectNoKK">
                        <?php foreach ($keluarga as $kk) : ?>
                            <option value="<?= $kk['no_kk']; ?>" data-kepala-keluarga="<?= $kk['kepala_keluarga']; ?>">
                                <?= $kk['kepala_keluarga'] . ' (' . $kk['no_kk'] . ')'; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                    <div class="form-group">
                        <label for="nama">Nama:</label>
                        <input type="text" name="nama" required class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="alamat">Alamat:</label>
                        <textarea name="alamat" required class="form-control"></textarea>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="tanggal_lahir">Tanggal Lahir:</label>
                            <input type="date" name="tanggal_lahir" required class="form-control">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="umur">Umur:</label>
                            <input type="number" name="umur" required class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="no_ktp">Nomor KTP:</label>
                        <input type="number" name="no_ktp" id="no_ktp" required class="form-control">
                        <small id="no_ktp_error" class="text-danger"></small>
                    </div>



                    <div class="form-group">
                        <label for="jenis_kelamin">Jenis Kelamin:</label>
                        <select name="jenis_kelamin" required class="form-control">
                            <option value="Laki-laki">Laki-laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="pekerjaan">pekerjaan:</label>
                        <input type="text" name="pekerjaan" required class="form-control"></input>
                    </div>
                    

                    <div class="form-group text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <input type="submit" class="btn btn-primary" id="simpanBtn" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


                        
                </div>
            </div>
          </div>
                     

</div>

</div>

<!-- fungsi untuk cek no_ktp jika ada yang sama -->

<script>
    $(document).ready(function() {
        <?php if ($this->session->flashdata('error')): ?>
            $('#tambahModal').modal('show');
        <?php endif; ?>

        // Fungsi untuk melakukan validasi nomor KTP secara real-time
        $('#no_ktp').on('focusout', function() {
            var no_ktp = $(this).val();
            $.ajax({
                type: 'POST',
                url: '<?= base_url('penduduk/cek_no_ktp'); ?>',
                data: {'no_ktp': no_ktp},
                dataType: 'json',
                success: function(response) {
                    if (response.status === 'error') {
                        $('#no_ktp_error').text(response.message);
                         $('#simpanBtn').prop('disabled', true);
                    } else {
                        $('#no_ktp_error').text('');
                        $('#simpanBtn').prop('disabled', false);
                    }
                }
            });
        });
    });
</script>


    

