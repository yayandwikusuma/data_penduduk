<!-- application/views/pdf_template.php -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Kelahiran</title>
    <style>
        body {
            font-family: Arial, sans-serif;
        }

        h1 {
            text-align: center;
            margin-bottom: 20px;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        table, th, td {
            border: 1px solid #ddd;
        }

        th, td {
            padding: 10px;
            text-align: left;
        }
    </style>
</head>
<body>

    <h1>Data Kelahiran</h1>

    <table>
        <thead>
            <tr>
                <th>No</th>
                 <th>Nama</th>
                <th>Tanggal Lahir</th>
                <th>Jenis Kelamin</th>
                <th>Keluarga</th>
               
            </tr>
        </thead>
        <tbody>
            <?php $i = 1; foreach ($kelahiran as $item): ?>
                <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $item['nama']; ?></td>
                    <td><?= date('d-m-Y', strtotime($item['tanggal_lahir'])); ?></td>
                    <td><?= $item['jenis_kelamin']; ?></td>
                    <td><?= $item['kepala_keluarga']; ?></td>
                    
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <script type= "text/javascript">
        window.print();
    </script>

</body>
</html>