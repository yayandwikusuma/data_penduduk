<!-- application/views/pdf_template.php -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Orang Pindah Rumah</title>
    <style>
        body {
            font-family: Arial, sans-serif;
        }

        h1 {
            text-align: center;
            margin-bottom: 20px;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        table, th, td {
            border: 1px solid #ddd;
        }

        th, td {
            padding: 10px;
            text-align: left;
        }
    </style>
</head>
<body>

    <h1>Data Orang Pindah Rumah</h1>

    <table>
        <thead>
            <tr>
                <th>No</th>
                 <th>Tanggal Pindah</th>
                <th>Alasan Pindah Rumah</th>
                <th>Alamat Baru</th>
               
            </tr>
        </thead>
        <tbody>
            <?php $i = 1; foreach ($pindah as $item): ?>
                <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $item['nama']; ?></td>
                    <td><?= date('d-m-Y', strtotime($item['tanggal_pindah'])); ?></td>
                    <td><?= $item['alasan']; ?></td>
                    <td><?= $item['alamat_baru']; ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <script type= "text/javascript">
        window.print();
    </script>

</body>
</html>