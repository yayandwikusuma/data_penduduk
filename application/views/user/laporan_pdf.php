<!-- application/views/pdf_template.php -->
<?php
ob_start();
?>
<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Penduduk</title>
    <style>
        body {
            font-family: Arial, sans-serif;
        }

        h1 {
            text-align: center;
            margin-bottom: 20px;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        table, th, td {
            border: 1px solid #ddd;
        }

        th, td {
            padding: 10px;
            text-align: left;
        }
    </style>
</head><body>

    <h1>Data Penduduk</h1>

    <table>
        <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Tanggal Lahir</th>
                <th>Umur</th>
                <th>No. KTP</th>
                <th>Jenis Kelamin</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 1; foreach ($penduduk as $p): ?>
                <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $p['nama']; ?></td>
                    <td><?= $p['alamat']; ?></td>
                    <td><?= date('d-m-Y', strtotime($p['tanggal_lahir'])); ?></td>
                    <td><?= $p['umur']; ?></td>
                    <td><?= $p['no_ktp']; ?></td>
                    <td><?= $p['jenis_kelamin']; ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

  

</body></html>