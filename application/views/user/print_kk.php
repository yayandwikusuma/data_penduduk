<!-- application/views/pdf_template.php -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Penduduk</title>
    <style>
        body {
            font-family: Arial, sans-serif;
        }

        h1 {
            text-align: center;
            margin-bottom: 20px;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        table, th, td {
            border: 1px solid #ddd;
        }

        th, td {
            padding: 10px;
            text-align: left;
        }
    </style>
</head>
<body>

    <h1>Data Penduduk</h1>

    <table>
        <thead>
            <tr>
                <th>No</th>
                 <th>No. KK</th>
                <th>Kepala Keluarga</th>
                <th>Alamat</th>
               
            </tr>
        </thead>
        <tbody>
            <?php $i = 1; foreach ($kk as $item): ?>
                <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $item['no_kk']; ?></td>
                    <td><?= $item['kepala_keluarga']; ?></td>
                    <td><?= $item['alamat']; ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <script type= "text/javascript">
        window.print();
    </script>

</body>
</html>