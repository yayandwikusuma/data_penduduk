  <!-- Begin Page Content -->
            <div class="container-fluid">

                    <!-- Page Heading -->
                     <!-- <h1 class="h3 mb-4 text-gray-800"><?= $title;?></h1>  -->
                      <h1 class="h3 mb-4 text-gray-800">Simulasi Angsuran Kredit</h1> 
                    <div class="portlet light bordered">

<form method= "post" action = " ">
                        <div class="row">
                        <div class="col-md-12">
                                <div class="col-md-12" >
                                        <table border="0">
                                            <tr>
                                            <td width="30%">Nama Mitra</td>
                                                <td align="center" width="10%">:</td>
                                                <td>
                                                    <input id="id_namaMitra" class="form-control input-sm" type="text" name="namaMitra" value = "Calon Mitra"/>
                                                
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">&nbsp;</td>  
                                                </tr>
                                            <tr>
                                            <td width="30%">Plafon (Rp) <span class="required"> * </span></td>
                                                <td align="center" width="10%">:</td>
                                                <td>
                                                    
                                                    <input id="id_plafon" class="form-control  input-sm nomor" type="text" name="plafon" />
                                                    
                                                
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">&nbsp;</td>  
                                                </tr>
                                        <tr>
                                                <td>Tanggal Akad <span class="required"> * </span></td>
                                                <td align="center">:</td>
                                                <td>
                                                    <input type="text" class="form-control input-tanggal" data-date-format="dd-mm-yyyy" name="tglAkad"  id="tglAkad" value ="<?=date('d-m-Y');?>">

                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">&nbsp;</td>  
                                                </tr>
                                        <tr>
                                            <td width="30%">Jasa (%) <span class="required"> * </span></td>
                                                <td align="center" width="10%">:</td>
                                                <td>
                                                    
                                                    <input id="jasa" class="form-control  input-sm nomor1" type="text" name="jasa" maxlength="2" value="3"/> 
                                                    
                                                
                                                </td>
                                            </tr>
                                            <tr>
                                            <tr>
                                        <td colspan="3">&nbsp;</td>  
                                        <tr>
                                            <td width="35%">Jangka Waktu (bln) <span class="required"> * </span></td>
                                                <td align="center" width="10%">:</td>
                                                <td>
                                                    
                                                    <input id="id_jangkaWaktu" class="form-control  input-sm nomor1" type="text" name="jangkaWaktu" maxlength="2" value="12" />
                                                    
                                                
                                                </td>
                                            </tr>
                                            <tr>
                                        <td colspan="3">&nbsp;</td>  
                                            <tr>
                                            <td width="30%">Grace Periode</td>
                                                <td align="center" width="10%">:</td>
                                                <td>
                                                    
                                                    <input id="id_gracePriod" class="form-control nomor1 input-sm" type="text" name="gracePriod"
                                                    maxlength="2" value = "0"/>
                                                
                                                </td>
                                            </tr>
  <tr>
                  <td colspan="3">&nbsp;</td>  
                    <tr>
                     <td  width="30%">Jenis Kredit <span class="required"> * </span></td>
                        <td align="center" width="10%">:</td>
                        <td>
                            <select id="id_jenisKredit" name="jenisKredit" class="form-control input-sm" onchange="jasa();">
                                <option value="12" >Flat Menurun Tahunan</option>
                                 <option value="7">Annuitas</option>
							    <option value="2" >Flat</option>
							    <option value="5">Flat Triwulan</option>
							    <option value="6">Flat Semester</option>
							    <option value="8">Flat Menurun awal tahun</option>
							    <option value="9">Pokok Semester (Menurun)</option>
                                <option value="10">Efektivitas</option>
							    {* <option value="10">Pokok Tahunan (Menurun)</option>  *}
                                     
                            </select>
                        </td>
                </tr>
                </tr>



                 <tr>
                  <td colspan="3">&nbsp;</td>  
                    <tr>
                     <td  width="30%"></td>
                        <td align="center" width="10%"></td>
                        <td>
                        <div class="form-group">
                            <button onclick="refresh_jadwal1()" name="btnFilter" class="btn blue" id="id_Filter">
                                Generate 
                            </button>
                            <button id="reset" type="button" class="btn default">Reset</button>
                            <button onclick="printDiv()" name="btnPrint" class="btn blue" id="id_Print" disabled="true">
                                Print 
                            </button>
                           <!--  <button id="id_refreshJadwal" onclick="refresh_jadwal1()" class="btn blue">Generate
                            </button> -->
                        </div>
                        </td>
                </tr>
                </tr>   
                                        </table>




                                    </div>
                        </div>
                        </div>
                        </div>

                </form>

 <br>

 

<div class="col-md-12">
    <div class="form-group">
    </div>
</div>
                        
<div class="portlet light bordered" id="tarea">
                        <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-body">
                                    <table class="table table-striped table-hover table-bordered"
                                           id="id_tabelJadwalBayar">
                                        <thead>
                                            <tr>
                                                <th width="5%">
                                                    No
                                                </th>
                                                <th width="15%">
                                                    Angsuran Ke
                                                </th>
                                                <th width="20%">
                                                    Tanggal Tagihan
                                                </th>
                                                <th width="20%">
                                                    Angsuran Pokok
                                                </th>
                                                <th width="20%">
                                                    Angsuran Jasa
                                                </th>
                                                <th width="20%">
                                                    Jumlah
                                                </th>

                                            </tr>
                                        </thead>
                                        <tbody id="id_body_data">

                                        </tbody>
                                    </table>

                                     <table class="table table-striped table-hover table-bordered"
                                           id="id_tabelJadwalBayar2">
                                        <thead>
                                            <tr>
                                                <th width="5%">
                                                    No
                                                </th>
                                                <th width="15%">
                                                    Angsuran Ke
                                                </th>
                                                 <th width="15%">
                                                    Tanggal Tagihan
                                                </th>
                                                <th width="15%">
                                                    Sisa Pokok
                                                </th>
                                                <th width="15%">
                                                    Pokok (a)
                                                </th>
                                                <th width="15%">
                                                    Jasa Adm (b)
                                                </th>
                                                <th width="20%">
                                                    Angsuran (a+b)
                                                </th>

                                            </tr>
                                        </thead>
                                        <tbody id="id_body_data2">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        </div>
</div>
  <table border="0" id="info" >
                    <tr>
                     <td width="30%">Nama Mitra</td>
                        <td align="center" width="10%">:</td>
                        <td><label id="nama"></label>
                         
                        </td>
                    </tr>
                    <tr>
                           <td colspan="3">&nbsp;</td>  
                           </tr>
                    <tr>
                     <td width="30%">Plafon (Rp) <span class="required"></span></td>
                        <td align="center" width="10%">:</td>
                        <td>
                            <label id="plafond"></label>

                        </td>
                    </tr>
                    <tr>
                           <td colspan="3">&nbsp;</td>  
                           </tr>
                  <tr>
                        <td>Tanggal Akad <span class="required"></span></td>
                        <td align="center">:</td>
                        <td>
                           <label id="tglakad"></label>
                        </td>
                    </tr>
                     <tr>
                           <td colspan="3">&nbsp;</td>  
                           </tr>
                   <tr>
                     <td width="30%">Jasa (%) <span class="required"></span></td>
                        <td align="center" width="10%">:</td>
                        <td>
                          <label id="jasas"></label>
                         
                        </td>
                    </tr>
                    <tr>
                    <tr>
                  <td colspan="3">&nbsp;</td>  
                   <tr>
                     <td width="35%">Jangka Waktu (bln) <span class="required"></span></td>
                        <td align="center" width="10%">:</td>
                        <td>
                        <label id="jw"></label>

                        </td>
                    </tr>
                    <tr>
                  <td colspan="3">&nbsp;</td>  
                    <tr>
                     <td width="30%">Grace Priod</td>
                        <td align="center" width="10%">:</td>
                        <td>
                        <label id="grace"></label>
                         
                        </td>
                    </tr>
                    <tr>
                  <td colspan="3">&nbsp;</td>  
                </tr>
                </table>

  <script type="text/javascript">
     $('#info').hide();

         function jasa () {
		var jw=$('#id_jangkaWaktu').val();
		var bp=$('#id_jenisKredit').val();
		if (bp=='2') {

		}else if(bp=='5'){
			if( (jw % 3)==0 ){

			}else{
				alert('Jangka waktu triwulan harus kelipatan 3');
			}
		}else if(bp=='6'){
			if( (jw % 6)==0 ){

			}else{
				alert('Jangka waktu triwulan harus kelipatan 6');

			}
		}else if(bp=='9'){
			if( (jw % 6)==0 ){
				$('#lptagihan2').show(); 

			}else{
				alert('Jangka waktu triwulan harus kelipatan 6');

			}
		}else if(bp=='10'){
			if( (jw % 12)==0 ){
				$('#lptagihan2').show(); 

			}else{
				alert('Jangka waktu triwulan harus kelipatan 12');

			}
		}else{
			$('#lptagihan2').hide(); 
		}
	};

        jQuery(document).ready(function () {
        readyToStart(); 
        $('#id_namaMitra').val('Calon Mitra');
        $('#id_jangkaWaktu').val('12');
        $('#tarea').hide();
        $('#id_tabelJadwalBayar').hide();
        $('#id_tabelJadwalBayar2').hide();
        $('.input-tanggal').datepicker({ orientation: "bottom", autoclose: !0, dateFormat: 'dd-mm-yy' });
        $('#jasa').val('3');
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        today = dd + '-' + mm + '-' + yyyy;
        $('#tglAkad').val(today);
        $('#id_plafond').focus();
     
        // document.getElementById("id_tabelJadwalBayar").style.display="none";
        // document.getElementById("id_tabelJadwalBayar2").style.display="none";
        //TableManaged();
    });


//function numbering
    function readyToStart(){
            $(".nomor").val("0.00");
            $(".nomor").focus(function(){
                if ($(this).val() == '0.00') {
                    $(this).val('');
                }else{
                    this.select();
                }
            });
            $(".nomor").focusout(function(){
                if ($(this).val() == '') {
                    $(this).val('0.00');
                }else{
                    var angka =$(this).val();
                    $(this).val(number_format(angka,2));
                }
            });
            $(".nomor").keyup(function(){
                var val = $(this).val();
                if(isNaN(val)){
                    val = val.replace(/[^0-9\.]/g,'');
                    if(val.split('.').length>2)
                        val =val.replace(/\.+$/,"");
                    }
                $(this).val(val);
                
            });
            
            $(".nomor1").val("0");
            $(".nomor1").focusout(function(){
                var val = $(this).val();
                if ($(this).val() == '') {
                    $(this).val('0');
                }else{
                    $(this).val(val);
                }
            });
            $(".nomor1").keyup(function(){
                var val = $(this).val();
                if(isNaN(val)){
                    val = val.replace(/[^0-9\.]/g,'');
                    if(val.split('.').length>2)
                        val =val.replace(/\.+$/,"");
                    }
                $(this).val(val);
                
            });
        };


    function number_format(number, decimals, dec_point, thousands_sep) {
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                s = '',
                toFixedFix = function (n, prec) {
                    var k = Math.pow(10, prec);
                    return '' + Math.round(n * k) / k;
                };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }

    //clean number
    function CleanNumber(value) {
        newValue = value.replace(/\,/g, '');
        return newValue;
    }
    
    // start coba
        function refresh_jadwal1() {

            if ( ($('#id_jangkaWaktu').val()<1 ) || ($('#id_jangkaWaktu').val()>72)) {
                alert('Maaf jangka waktu yang diizinkan antara 1 bulan sampai 72 bulan');
            }else{
                 if($('#id_jenisKredit').val()=='5'){ //jika triwulan
                    if(  ($('#id_jangkaWaktu').val() % 3) ){
                         alert('Maaf jangka waktu untuk triwulan minimal 3 bulan, dengan kelipatan 3');
                    }else{
                       //isi triwulan :open header table 2 
                       isi_table($('#id_jenisKredit').val()); 
                    }
                 }else if($('#id_jenisKredit').val()=='6'){
                    //isi smester :open header table 2 
                    if(  ($('#id_jangkaWaktu').val() % 6) ){
                         alert('Maaf jangka waktu untuk triwulan minimal 6 bulan, dengan kelipatan 6');
                    }else{
                       //isi triwulan :open header table 2 
                       isi_table($('#id_jenisKredit').val()); 
                    }
                 }else{
                    //isi flat :open header table 1 
                    isi_table($('#id_jenisKredit').val());
                 }
                
            }    
    };
   
    
    function isi_table($jenis_kredit){


        if($jenis_kredit=='10') { 
            var bunga = jasa;
            var realisasi = id_plafon;
            var gp_static = 0; // TJSLASK-152
            var jkWaktuWithoutGrace = $id_jangkaW - parseInt($id_gracePriod);
            var $jmlAngs = $id_jangkaW;
			var jkWaktu = parseInt($id_jangkaW) ;//+ parseInt($id_gracePriod); // TOTAL JANGKA WAKTU = JANGKA WAKTU + JML GRACE PERIODE (BULAN) - TJSLASK-152
			var sukubunga_efektif = (parseInt(bunga)/100)/12;//($this->tofloat($bunga)/100)/12; // SUKU BUNGA EFEKTIF PER BULAN
            var f = 1+sukubunga_efektif
            var o4 = Math.pow(f,jkWaktuWithoutGrace);
            var o5 = 1-1/o4;
            var totalAngs = Math.round(Math.round(Math.ceil((parseFloat(realisasi)*(parseFloat(bunga)/100))/12), -3) / o5,0);	
            var x = 1;
            var n =0;
            var tot = 0;
            var bln = 1;
            var plafond = parseFloat(realisasi);
            var sisaPokok = plafond;
            var angsuranPokok = realisasi/jkWaktu;

            var d= tglAkad.substr(0,2);
            
            var m= tglAkad.substr(3,2);
            var y= tglAkad.substr(6,4);
            var tr= "";
            
            var total_angsuran_ke = 0;
            var total_jasa = 0;
            var total_jumlah = 0;

            for (var i = 1; i <= $jmlAngs; i++) {
                //set tgl
                m=parseFloat(m)+1;
                if(m>12){
                    y=parseFloat(y)+1;
                    m=parseFloat(m)-12;
                }
                if(m<10){
                    m="0"+m;
                }
                tglAkad=d+"-"+m+"-"+y;
                if(i <= $id_gracePriod){
                    jasaBulanan = 0;
					pokokBulanan = 0;
					angsuran = 0;

					 bMonthlyJasa_X =0;//bMonthlyJasa_X = Math.round((Math.ceil((parseFloat(plafond)*(parseFloat(bunga)/100))/12)/1000)*1000, -3);
					// //$angsuran_X = $bMonthlyPokok_X + $bMonthlyJasa_X;
                    angsuran = 0;
					jasaBulanan = 0;
                    

                }else{
                pokokBulanan = angsuranPokok;
                jasaBulanan = Math.round((plafond - ((i-1)*pokokBulanan))* (parseFloat(bunga)/100)/12);
                  sisaPokok = sisaPokok- pokokBulanan;  
                }

                if(i == $id_jangkaW){
					
					pokokBulanan += sisaPokok;
                    sisaPokok = 0;
				};	

                angsuran_pokok_ke = pokokBulanan;
                jasaB = jasaBulanan;
                jumlah_ke = angsuran_pokok_ke + jasaB;

                tr += '<tr class="listdata" id="tr' + i + '">';
                tr += '<td>' + i + '</td>';
                tr += '<td><input type="text" class="form-control input-sm" id="id_tempNamaPerk' + i + '" name="tempNamaPerk' + i + '" readonly="true" value="'  + i+ '"></td>';
                tr += '<td><input type="text" class="form-control input-sm" id="id_tempKet' + i + '" name="tempKet' + i + '" readonly="true" value="' + tglAkad+ '"></td>';
                tr += '<td><input type="text" class="form-control nomor input-sm" id="id_tempNamaPerk' + i + '" name="tempKodePerk' + i + '" readonly="true" value="' + number_format(angsuran_pokok_ke)  +'"></td>';
                tr += '<td><input type="text" class="form-control nomor input-sm" id="id_tempNamaPerk' + i + '" name="tempNamaPerk' + i + '" readonly="true" value="' + number_format(jasaB) + '"></td>';
                tr += '<td><input type="text" class="form-control nomor input-sm" id="id_tempNamaPerk' + i + '" name="tempNamaPerk' + i + '" readonly="true" value="' + number_format(jumlah_ke)+ '"></td>';
                tr += '</tr>';
                 total_angsuran_ke += angsuran_pokok_ke;
                 total_jasa += jasaB;
                 total_jumlah += jumlah_ke;

            }    

            tr += '<tr class="listdata" id="tr' + i + '">';
            tr += '<td colspan="3" align="Center" >Total</td>';
            tr += '<td align="right">' +number_format(total_angsuran_ke)+'</td>';
            tr += '<td align="right">'+number_format(total_jasa)+'</td>';
            tr += '<td align="right">'+number_format(total_jumlah)+'</td>';
            tr += '</tr>';

            $('#id_body_data').append(tr);
            $('#id_body_data2').append(tr);

            
            
        }

        $('#id_Print').attr('disabled', false);
    }


