
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800"><?= $title;?></h1>



                    
<nav class="navbar navbar-expand-lg navbar-light bg-light">

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
      <button type="button" class="btn btn-primary mb-3 mr-2" data-toggle="modal" data-target="#tambahModal" >Tambah Kematian</button>
      </li>
      <li class="nav-item">
      <a class= "btn btn-danger mr-2" href="<?php echo base_url('kematian/print_kematian')?>"><i class="fa fa-print"></i> Print</a>
      </li>
       <li class="nav-item">
      <a class= "btn btn-warning" href="<?php echo base_url('kematian/pdf')?>"><i class="fa fa-file"></i> Export PDF</a>
      </li>



    </ul>
    <div class="form-inline my-2 my-lg-2">
    <?php echo form_open('kematian/cari_data')?>
      <input type="text" class="form-control mr-sm-2"  placeholder="Search" aria-label="Search" name= "keyword">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Cari</button>
      <?php echo form_close()?>
    </div>
  </div>
</nav>

                        <div class="card shadow mb-4">
                        <div class="card-header py-3">
                             <div class="d-flex justify-content-between align-items-center">
                            <h6 class="m-0 font-weight-bold text-primary mb-3"  style="font-size: 20px;">Data Kematian </h6>
                                                  
                        </div>
                      </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered " id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                         <tr>
                                         <th style="width: 5%;">No.</th>
                                            <th style="width: 15%; text-align: center;" >Nama </th>
                                            <th style="width: 12%; text-align: center;" >Tgl Meninggal </th>
                                            <th style="width: 5%; text-align: center;" >Sebab</th>
                                            <th style="width: 10%; text-align: center;" >Pemakaman</th>
                                             <th style="width: 13%; text-align: center;" >Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; ?>
                                          <?php foreach ($kematian as $item) : ?>
                                        <tr>
                                        <th scope="row"><?= $i; ?></th>
                                        <td><?= $item['nama']; ?></td>
                                        <td><?= date('d-m-Y', strtotime($item['tanggal_kematian'])); ?></td>
                                        <td><?= $item['sebab_kematian']; ?></td>
                                        <td><?= $item['alamat_pemakaman']; ?></td>
                                        <td>
                                            <a href="#" class="badge badge-primary" data-toggle="modal" data-target="#editModal<?= $item['id']; ?>">Edit</a>
                                            <a href="<?= base_url('kematian/hapus_kematian/' . $item['id']); ?>" class="badge badge-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Hapus</a>
                                        </td>
                                        </tr>
                                    <?php $i++; ?>



           
        
                  <!-- Modal Edit Kematian -->
<div class="modal fade" id="editModal<?= $item['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="editModalLabel<?= $item['id']; ?>" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel<?= $item['id']; ?>">Edit Kematian</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url('kematian/edit_kematian'); ?>" method="post">
                    <input type="hidden" name="id" value="<?= $item['id']; ?>">


                      <div class="form-group">
                        <label for="nama">Pilih Nama:</label>
                        <select name="nama" id="editNama<?= $item['id']; ?>" class="form-control">
                            <?php foreach ($penduduk as $p) : ?>
                                <option value="<?= $p['nama']; ?>" <?= ($item['nama'] == $p['nama']) ? 'selected' : ''; ?>><?= $p['nama']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tanggal_kematian">Tanggal Kematian:</label>
                        <input type="date" name="tanggal_kematian" id="editTanggalKematian<?= $item['id']; ?>" value="<?= $item['tanggal_kematian']; ?>" required class="form-control">
                    </div>
                     <div class="form-group">
                        <label for="sebab_kematian">Sebab:</label>
                        <input type="text" name="sebab_kematian" id="editSebabKematian<?= $item['id']; ?>" value="<?= $item['sebab_kematian']; ?>" required class="form-control">
                    </div>
                     <div class="form-group">
                        <label for="alamat_pemakaman">alamat pemakaman:</label>
                        <input type="text" name="alamat_pemakaman" id="editAlamatPemakaman<?= $item['id']; ?>" value="<?= $item['alamat_pemakaman']; ?>" required class="form-control">
                    </div>
                    <!-- <div class="form-group">
                        <label for="kepala_keluarga">Kepala Keluarga:</label>
                        <select name="kepala_keluarga" id="editKepalaKeluarga<?= $item['id']; ?>" class="form-control">
                            <?php foreach ($keluarga as $kk) : ?>
                                <option value="<?= $kk['kepala_keluarga']; ?>" <?= ($item['kepala_keluarga'] == $kk['kepala_keluarga']) ? 'selected' : ''; ?>><?= $kk['kepala_keluarga']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div> -->
                    <div class="form-group text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <input type="submit" class="btn btn-primary" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

            <?php endforeach; ?>
    </tbody>
</table>




                <div class="modal fade" id="tambahModal" tabindex="-1" role="dialog" aria-labelledby="tambahModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="tambahModalLabel">Tambah data meninggal</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                            <form action="<?= base_url('kematian/tambah_kematian'); ?>" method="post">
                            
                            <select name="nama" required class="form-control">
                            <option value="">Pilih Nama</option>
                            <?php foreach ($penduduk as $p) : ?>
                                <option value="<?= $p['nama']; ?>"><?= $p['nama']; ?></option>
                            <?php endforeach; ?>
                            </select>
                                <div class="form-group">
                                    <label for="tanggal_kematian">Tanggal Kematian:</label>
                                    <input type="date" name="tanggal_kematian" required class="form-control">
                                </div>
                                 <div class="form-group">
                                    <label for="sebab">Sebab:</label>
                                    <input type="text" name="sebab" required class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="alamat_pemakaman">Pemakaman:</label>
                                    <input type="text" name="alamat_pemakaman" required class="form-control">
                                </div>
                                <!-- <div class="form-group">
                                <label for="kepala_keluarga">Kepala Keluarga:</label>
                                <select name="kepala_keluarga" class="form-control">
                                    <?php foreach ($keluarga as $kk) : ?>
                                        <option value="<?= $kk['kepala_keluarga']; ?>"><?= $kk['kepala_keluarga']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div> -->
                                <div class="form-group text-right">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                    <input type="submit" class="btn btn-primary" value="Simpan">
                                </div>
                            </form>
                        </div>
                        </div>
                    </div>
                </div>

                </div>
            </div>
          </div>

         
          </div>
     </div>

          <script src="<?php echo base_url() ?>assets/vendor/jquery/jquery.min.js"></script>


<script>
    $(document).ready(function () {
        // Menggunakan AJAX untuk mengambil data nama dari tabel penduduk
        $.ajax({
            type: 'POST',
            url: '<?= base_url('kematian/get_nama_penduduk'); ?>',
            success: function (response) {
                // Memasukkan hasil AJAX ke dalam elemen dengan id "nama"
                $('#nama').html(response);
            }
        });
    });
</script>