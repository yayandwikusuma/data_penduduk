
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800"><?= $title;?></h1>



                    
<nav class="navbar navbar-expand-lg navbar-light bg-light">

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
      <button type="button" class="btn btn-primary mb-3 mr-2" data-toggle="modal" data-target="#tambahModal" >Tambah Pindah Rumah</button>
      </li>
      <li class="nav-item">
      <a class= "btn btn-danger mr-2" href="<?php echo base_url('pindah/print_pindah')?>"><i class="fa fa-print"></i> Print</a>
      </li>
       <li class="nav-item">
      <a class= "btn btn-warning" href="<?php echo base_url('pindah/pdf')?>"><i class="fa fa-file"></i> Export PDF</a>
      </li>



    </ul>
    <div class="form-inline my-2 my-lg-2">
    <?php echo form_open('pindah/cari_data')?>
      <input type="text" class="form-control mr-sm-2"  placeholder="Search" aria-label="Search" name= "keyword">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Cari</button>
      <?php echo form_close()?>
    </div>
  </div>
</nav>


                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                             <div class="d-flex justify-content-between align-items-center">
                            <h6 class="m-0 font-weight-bold text-primary mb-3"  style="font-size: 20px;">Data Kematian </h6>
                                                  
                        </div>
                      </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered " id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                         <tr>
                                         <th style="width: 5%;">No.</th>
                                            <th style="width: 15%; text-align: center;" >Nama </th>
                                            <th style="width: 10%; text-align: center;" >Tgl Pindah Rumah </th>
                                            <th style="width: 18%; text-align: center;" >Alasan Pindah</th>
                                            <th style="width: 17%; text-align: center;" >Alamat Baru</th>
                                             <th style="width: 13%; text-align: center;" >Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; ?>
                                          <?php foreach ($pindah as $item) : ?>
                                        <tr>
                                            <th scope="row"><?= $i; ?></th>
                                            <td><?= $item['nama']; ?></td>
                                            <td><?= date('d-m-Y', strtotime($item['tanggal_pindah'])); ?></td>
                                            <td><?= $item['alasan']; ?></td>
                                            <td><?= $item['alamat_baru']; ?></td>
                                            <td>
                                                <a href="#" class="badge badge-primary" data-toggle="modal" data-target="#editModal<?= $item['id']; ?>">Edit</a>
                                                <a href="<?= base_url('pindah/hapus_pindah/' . $item['id']); ?>" class="badge badge-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Hapus</a>
                                            </td>
                                        </tr>
                                    <?php $i++; ?>





           
        
                                    <!-- Modal Edit Pindah Rumah -->
                    <div class="modal fade" id="editModal<?= $item['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="editModalLabel<?= $item['id']; ?>" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="editModalLabel<?= $item['id']; ?>">Edit Pindah Rumah</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="<?= base_url('pindah/edit_pindah'); ?>" method="post">
                                        <input type="hidden" name="id" value="<?= $item['id']; ?>">


                                        <div class="form-group">
                                            <label for="nama">Pilih Nama:</label>
                                            <select name="nama" id="editNama<?= $item['id']; ?>" class="form-control">
                                                <?php foreach ($penduduk as $p) : ?>
                                                    <option value="<?= $p['nama']; ?>" <?= ($item['nama'] == $p['nama']) ? 'selected' : ''; ?>><?= $p['nama']; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="tanggal_pindah">Tanggal Pindah Rumah:</label>
                                            <input type="date" name="tanggal_pindah" id="editTanggalPindah<?= $item['id']; ?>" value="<?= $item['tanggal_pindah']; ?>" required class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="alasan">Alasan Pindah:</label>
                                            <input type="text" name="alasan" id="editAlasan<?= $item['id']; ?>" value="<?= $item['alasan']; ?>" required class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="alamat_baru">Alamat Baru:</label>
                                            <input type="text" name="alamat_baru" id="editAlamatBaru<?= $item['id']; ?>" value="<?= $item['alamat_baru']; ?>" required class="form-control">
                                        </div>
                                        <div class="form-group text-right">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                            <input type="submit" class="btn btn-primary" value="Simpan">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

            <?php endforeach; ?>
    </tbody>
</table>


<!-- Modal Tambah Pindah Rumah -->

                <div class="modal fade" id="tambahModal" tabindex="-1" role="dialog" aria-labelledby="tambahModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="tambahModalLabel">Tambah Pindah Rumah</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                            <form action="<?= base_url('pindah/tambah_pindah'); ?>" method="post">
                            
                            <select name="nama" required class="form-control">
                            <option value="">Pilih Nama</option>
                            <?php foreach ($penduduk as $p) : ?>
                                <option value="<?= $p['nama']; ?>"><?= $p['nama']; ?></option>
                            <?php endforeach; ?>
                            </select>
                                <div class="form-group">
                                    <label for="tanggal_pindah">Tanggal Pindah Rumah:</label>
                                    <input type="date" name="tanggal_pindah" required class="form-control">
                                </div>
                                 <div class="form-group">
                                    <label for="alasan">Alasan Pindah:</label>
                                    <input type="text" name="alasan" required class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="alamat_baru">Alamat Baru:</label>
                                    <input type="text" name="alamat_baru" required class="form-control">
                                </div>
                                <div class="form-group text-right">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                    <input type="submit" class="btn btn-primary" value="Simpan">
                                </div>
                            </form>
                        </div>
                        </div>
                    </div>
                </div>

                </div>
            </div>
          </div>


         
    </div>
</div>




