
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800"><?= $title;?></h1>


                    <!-- <h2>Data Penduduk</h2> -->
                    <!-- Tombol Tambah Penduduk -->


<!-- <button type="button" class="btn btn-primary mb-3 mr-3" data-toggle="modal" data-target="#tambahModal">Tambah Penduduk</button> -->


<nav class="navbar navbar-expand-lg navbar-light bg-light">

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
      <button type="button" class="btn btn-primary mb-3 mr-2" data-toggle="modal" data-target="#tambahModal" >Tambah KK</button>
      </li>
      <li class="nav-item">
      <a class= "btn btn-danger mr-2" href="<?php echo base_url('kk/print_kk')?>"><i class="fa fa-print"></i> Print</a>
      </li>
       <li class="nav-item">
      <a class= "btn btn-warning" href="<?php echo base_url('kk/pdf')?>"><i class="fa fa-file"></i> Export PDF</a>
      </li>



    </ul>
    <div class="form-inline my-2 my-lg-2">
    <?php echo form_open('kk/cari_data')?>
      <input type="text" class="form-control mr-sm-2"  placeholder="Search" aria-label="Search" name= "keyword">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Cari</button>
      <?php echo form_close()?>
    </div>
  </div>
</nav>




     <div class="card shadow mb-4">
                        <div class="card-header py-3">
                             <div class="d-flex justify-content-between align-items-center">
                            <h6 class="m-0 font-weight-bold text-primary mb-3"  style="font-size: 20px;">Penduduk </h6>
                                                  
                        </div>
        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered " id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                        <th style="width: 5%;">No.</th>
                                            <th style="width: 15%; text-align: center;" >No.KK </th>
                                            <th style="width: 20%;text-align: center;">Kepala Keluarga </th>
                                            <th style="width: 45%; text-align: center;" >Alamat </th>
                                             <th style="width: 10%; text-align: center;" >Aksi</th>


                                        </tr>
                                    </thead>
                                    <tbody>
                                           <?php $i = 1; ?>
                                          <?php foreach ($kk as $item) : ?>
                                        <tr>
                                         <th scope="row"><?= $i; ?></th>
                                        <td style= "text-align: justify"><?= $item['no_kk']; ?></td>
                                        <td style= "text-align: justify"><?= $item['kepala_keluarga']; ?></td>
                                        <td style= "text-align: left"><?= $item['alamat']; ?></td>


                                        <td>
                                         <a href="#" class="badge badge-success" data-toggle="modal" data-target="#ViewModal<?= $item['id']; ?>">View</a>
                                         <a href="#" class="badge badge-primary" data-toggle="modal" data-target="#editModal<?= $item['id']; ?>">Edit</a> 
                                         <a href="<?= base_url('kk/delete_kk/' . $item['id']); ?>" class="badge badge-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Hapus</a>
                                        </td>
                                        </tr>
                                    <?php $i++; ?>

                                     <!-- Formulir Edit KK -->            


                                   <div class="modal fade" id="editModal<?= $item['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="editModalLabel<?= $item['id']; ?>" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="editModalLabel<?= $item['id']; ?>">Edit KK</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body"> 
                                                            <!-- Formulir Edit KK -->
                                                            <form action="<?= base_url('KK/update_kk'); ?>" method="post">
                                                                <input type="hidden" name="id" value="<?= $item['id']; ?>">
                                                                <div class="form-group">
                                                                    <label for="no_kk">Nomor KK:</label>
                                                                    <input type="text" name="no_kk" value="<?= $item['no_kk']; ?>" required class="form-control" >
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="kepala_keluarga">Kepala Keluarga:</label>
                                                                    <input type="text" name="kepala_keluarga" value="<?= $item['kepala_keluarga']; ?>" required class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="alamat">Alamat:</label>
                                                                    <textarea name="alamat" required class="form-control"><?= $item['alamat']; ?></textarea>
                                                                </div>
                                                                    <div class="form-group">
                                                                    <label for="anggota_kk">ANGGOTA KELUARGA:</label>
                                                                    
                                                                   
                                                                    <?php
                                                                    $anggota_kk = $this->Penduduk_model->get_anggota_kk_by_no_kk($item['no_kk']);
                                                                    foreach ($anggota_kk as $anggota) : ?>
                                                                        <p>Nama: <?= $anggota['nama']; ?></p>
                                                                      
                                                                    <?php endforeach; ?>
                                                                    </div>
                                                                
                                                                <div class="form-group text-right">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                                    <input type="submit" class="btn btn-primary" value="Simpan Perubahan">
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                <?php endforeach; ?>
    </tbody>
</table>

                                    <!-- Modal view anggota KK -->
                                    <?php foreach ($kk as $item) : ?>
                                        <!-- Modal view anggota KK -->
                                        <div class="modal fade" id="ViewModal<?= $item['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="ViewModalLabel<?= $item['id']; ?>" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="ViewModalLabel<?= $item['id']; ?>" >View Anggota Keluarga</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <label for="kepala_keluarga">Kepala Keluarga:</label>
                                                            <h3 style="text-align: center"><?= $item['kepala_keluarga']; ?></h3>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="anggota_kk">ANGGOTA KELUARGA:</label>
                                                            <table class="table table-bordered">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width: 3%; text-align: center;">No</th>
                                                                        <th style="width: 25%; text-align: center;">Nama</th>
                                                                        <!-- Tambahkan kolom lain jika diperlukan -->
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="anggotaTable<?= $item['id']; ?>">
                                                                    <!-- Isi tabel dengan JavaScript -->
                                                                    <?php
                                                                    $anggota_kk = $this->Penduduk_model->get_anggota_kk_by_no_kk($item['no_kk']);
                                                                    $nomor = 1;
                                                                    foreach ($anggota_kk as $anggota) : ?>
                                                                        <tr>
                                                                            <td style= "text-align: justify"><?= $nomor++; ?></td>
                                                                            <td><?= $anggota['nama']; ?></td>
                                                                            <!-- Tambahkan kolom lain jika diperlukan -->
                                                                        </tr>
                                                                    <?php endforeach; ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                
          
                        <!-- Modal Tambah KK -->
                        <div class="modal fade" id="tambahModal" tabindex="-1" role="dialog" aria-labelledby="tambahModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="tambahModalLabel">Tambah KK</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <!-- Formulir Tambah KK -->
                                        <form action="<?= base_url('kk/add_kk'); ?>" method="post">
                                            <div class="form-group">
                                             <label for="no_kk">Nomor KK:</label>
                                            <input type="text" name="no_kk" id="no_kk" required class="form-control">
                                            <small id="no_kk_message" class="text-danger"></small>
                                            </div>
                                            <div class="form-group">
                                            
                                                <label for="kepala_keluarga">Kepala Keluarga:</label>
                                                <input type="text" name="kepala_keluarga" required class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="alamat">Alamat:</label>
                                                <textarea name="alamat" required class="form-control"></textarea>
                                            </div>
                                            <div class="form-group text-right">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                <input type="submit" class="btn btn-primary" value="Simpan" id="simpanBtn" disabled>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

            
                </div>
            </div>
          </div>
                     

           <!-- End of Main Content -->

</div>
                <!-- /.container-fluid -->

</div>


<!--  jQuery untuk pengecekan realtime -->
<script>
    $(document).ready(function() {
        <?php if ($this->session->flashdata('error')): ?>
            $('#tambahModal').modal('show');
        <?php endif; ?>

        // Fungsi untuk melakukan validasi nomor KK secara real-time
        $('#no_kk').on('focusout', function() {
            var no_kk = $(this).val();
            $.ajax({
                type: 'POST',
                url: '<?= base_url('kk/check_duplicate'); ?>',
                data: {'no_kk': no_kk},
                dataType: 'text',
                success: function(response) {
                    if (response === 'duplicate') {
                        $('#no_kk_message').text('Nomor KK sudah terdaftar.');
                        $('#simpanBtn').prop('disabled', true);
                    } else {
                        $('#no_kk_message').text('');
                        $('#simpanBtn').prop('disabled', false);
                    }
                }
            });
        });
    });
</script>