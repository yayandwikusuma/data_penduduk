  <!-- Begin Page Content -->
            <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800"><?= $title;?></h1>
                    <div class="portlet light bordered">

<form method= "post" action = "<?= base_url('simulasi/kredit');?> ">
                        <div class="row">
                        <div class="col-md-12">
                                <div class="col-md-12" >
                                        <table border="0">
                                            <tr>
                                            <td width="30%">Nama Mitra</td>
                                                <td align="center" width="10%">:</td>
                                                <td>
                                                    <input id="id_namaMitra" class="form-control input-sm" type="text" name="namaMitra" value = "Calon Mitra"/>
                                                
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">&nbsp;</td>  
                                                </tr>
                                            <tr>
                                            <td width="30%">Plafon (Rp) <span class="required"> * </span></td>
                                                <td align="center" width="10%">:</td>
                                                <td>
                                                    
                                                    <input id="id_plafon" class="form-control  input-sm nomor" type="text" name="plafon" />
                                                    
                                                
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">&nbsp;</td>  
                                                </tr>
                                        <tr>
                                                <td>Tanggal Akad <span class="required"> * </span></td>
                                                <td align="center">:</td>
                                                <td>
                                                    <input type="text" class="form-control input-tanggal" data-date-format="dd-mm-yyyy" name="tglAkad"  id="tglAkad" value ="<?=date('d-m-Y');?>">

                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">&nbsp;</td>  
                                                </tr>
                                        <tr>
                                            <td width="30%">Jasa (%) <span class="required"> * </span></td>
                                                <td align="center" width="10%">:</td>
                                                <td>
                                                    
                                                    <input id="jasa" class="form-control  input-sm nomor1" type="text" name="jasa" maxlength="2" value="3"/> 
                                                    
                                                
                                                </td>
                                            </tr>
                                            <tr>
                                            <tr>
                                        <td colspan="3">&nbsp;</td>  
                                        <tr>
                                            <td width="35%">Jangka Waktu (bln) <span class="required"> * </span></td>
                                                <td align="center" width="10%">:</td>
                                                <td>
                                                    
                                                    <input id="id_jangkaWaktu" class="form-control  input-sm nomor1" type="text" name="jangkaWaktu" maxlength="2" value="12" />
                                                    
                                                
                                                </td>
                                            </tr>
                                            <tr>
                                        <td colspan="3">&nbsp;</td>  
                                            <tr>
                                            <td width="30%">Grace Periode</td>
                                                <td align="center" width="10%">:</td>
                                                <td>
                                                    
                                                    <input id="id_gracePriod" class="form-control nomor1 input-sm" type="text" name="gracePriod"
                                                    maxlength="2" value = "0"/>
                                                
                                                </td>
                                            </tr>
                                            <tr>
                                        <td colspan="3">&nbsp;</td>  
                                            <tr>
                                            <td  width="30%">Jenis Kredit <span class="required"> * </span></td>
                                                <td align="center" width="10%">:</td>
                                                <td>
                                        <select id="id_jenisKredit" name="jenisKredit" class="form-control input-sm" >
                                        <option value="1">Efektivitas</option>
                                        <option value="2" >Flat</option>
                                        <!-- <option value="3" >Flat Menurun Tahunan</option>
                                        <option value="4">Annuitas</option>
                                        <option value="5">Flat Triwulan</option>
                                        <option value="6">Flat Semester</option>
                                        <option value="7">Flat Menurun awal tahun</option>
                                        <option value="8">Pokok Semester (Menurun)</option>                                   -->
                            </select>
                                                    <!-- <input id="id_jenisKredit" name="jenisKredit" class="form-control input-sm" type="text" value "Efektivitas"/> -->
                                                        
                                                        <!-- <option value="efektivitas">Efektivitas</option> -->
                                                        
                                                            
                                            
                                                </td>
                                        </tr>
                                        </tr>

                                        <tr>
                                        <td colspan="3">&nbsp;</td>  
                                            <tr>
                                            <td  width="30%"></td>
                                                <td align="center" width="10%"></td>
                                                <td>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary" name="btnFilter" class="btn blue" id="id_Filter">
                                                        Generate 
                                                    </button>
                                                    <!-- <button id="reset" type="button" class="btn default">Reset</button>
                                                    <button onclick="printDiv()" name="btnPrint" class="btn blue" id="id_Print" disabled="true">
                                                        Print 
                                                    </button> -->
                                                <!--  <button id="id_refreshJadwal" onclick="refresh_jadwal1()" class="btn blue">Generate
                                                    </button> -->
                                                </div>
                                                </td>
                                        </tr>
                                        </tr>   
                                        </table>




                                    </div>
                        </div>
                        </div>
                        </div>

</form>

            </div>
<hr>


<?php if(isset($angsuranList)) { ?>
    
	<table class="table table-striped">
		<thead>
			<tr>
				<th>NO</th>
				<th>Angsuran ke-</th>
				<th>Tanggal Tagihan</th>
				<th>Angsuran Pokok</th>
				<th>Jasa Angsuran</th>
				<th>Jumlah</th>
			</tr>
		</thead>
		<tbody>
			<?php 
				$i = 1;
				foreach($angsuranList as $angsuran) {
					$totalPokok += $angsuran->pokok;
					$totalAngsuran += $angsuran->angsuran;
					$totalJasaAngsuran += $angsuran->jasa;
			?>
			<tr>
				<td><?php echo $i++; ?></td>
				<td><?php echo $angsuran->angsuran_ke; ?></td>
				<td><?php echo $angsuran->tgl_tagihan; ?></td>
				<td><?php echo number_format($angsuran->pokok, 2); ?></td>
				<td><?php echo number_format($angsuran->jasa, 2); ?></td>
				<td><?php echo number_format($angsuran->angsuran, 2); ?></td>
			</tr>
			<?php } ?>
			<tr>
				<th colspan="3">Total:</th>
				<th><?php echo number_format($totalPokok,2); ?></th>
            <th><?php echo number_format($totalJasaAngsuran, 2); ?></th>
            <th><?php echo number_format($totalAngsuran, 2); ?></th>
            </tr>
            </table>
            <?php
                }
                ?>
            </div>

            </div>

            <!-- End of Main Content -->
