
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800"><?= $title;?></h1>



                    
<nav class="navbar navbar-expand-lg navbar-light bg-light">

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
<!-- 
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
      <button type="button" class="btn btn-primary mb-3 mr-2" data-toggle="modal" data-target="#tambahModal1" >Tambah judul</button>
      </li>
     -->  
</nav>

    <!--  Unggulan satu -->

                     <div class="card shadow mb-4">
                        <div class="card-header py-3">
                             <div class="d-flex justify-content-between align-items-center">
                            <h6 class="m-0 font-weight-bold text-primary mb-3"  style="font-size: 20px;">Kategori </h6>
                             <button type="button" class="btn btn-primary mb-0 mr-2" data-toggle="modal" data-target="#tambahModal">Tambah Kategori</button>
                             </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered " id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                        <th style="width: 5%;">No.</th>
                                            <th style="width: 60%; text-align: center;" >Nama Kategory </th>
                                            <th style="width: 30%;text-align: center;">Aksi </th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                          <?php
                                        foreach ($kategori as $item) : ?>
                                        <tr>
                                        <th scope="row"><?= $item['id']; ?></th>

                                        <td style= "text-align: justify"><?= $item['nama_kategori']; ?></td> 
                                        <td>
                                         <a href="#" class="badge badge-primary" data-toggle="modal" data-target="#editModalKategori<?= $item['id']; ?>">Edit</a>
                                          <a href="<?= base_url('gallery/hapus_kategori/' . $item['id']); ?>" class="badge badge-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Hapus</a>
                                        </td>
                                        </tr>



                     <!-- Modal Edit Kategori -->
                                            <div class="modal fade" id="editModalKategori<?= $item['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="editModalLabelKategori<?= $item['id']; ?>" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="editModalLabelKategori<?= $item['id']; ?>">Edit Kategori</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                          <form action="<?= base_url('Gallery/edit_kategori/' . $item['id']); ?>" method="post">
                                                                <input type="hidden" name="id" valuSe="<?= $item['id']; ?>">

                                                                <div class="form-group">
                                                                    <label for="nama_kategori">Kategori :</label>
                                                                    <input type="text" name="nama_kategori" value="<?= $item['nama_kategori']; ?>" required class="form-control">
                                                                </div>

                                                                <div class="form-group text-right">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                                    <input type="submit" class="btn btn-primary" value="Simpan">
                                                                </div>
                                                                </form>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                            <?php  endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    


                    <!-- Modal Tambah Kategori -->
                        <div class="modal fade" id="tambahModal" tabindex="-1" role="dialog" aria-labelledby="tambahModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="tambahModalLabel">Tambah Kategori</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <!-- Formulir Tambah Penduduk -->
                                        <form action="<?= base_url('Gallery/tambah_kategori/'); ?>" method="post">
                                            <div class="form-group">
                                                <label for="nama_kategori">Nama Kategori:</label>
                                                <input type="text" name="nama_kategori" required class="form-control">
                                            </div>
                                            <div class="form-group text-right">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                <input type="submit" class="btn btn-primary" value="Simpan">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                        <!--  Gallery Gambar -->

                    <!-- <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Gallery </h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered " id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                        <th style="width: 5%;">No.</th>
                                            <th style="width: 16%; text-align: center;" >Gambar</th>
                                            <th style="width: 30%;text-align: center;">Aksi</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                          <?php
                                        foreach ($gallery as $item) : ?>
                                        <tr>
                                        <th scope="row"><?= $item['id']; ?></th>

                                        <td> <img src="<?= base_url('./gambar/gallery/' . $item['gambar']); ?>" alt="Gambar" width="100" style="display: block; margin: 0 auto; max-width: 200px; max-height: 1000px; width: auto; height: auto;"></td>
                                        
                                        <td>
                                         <a href="#" class="badge badge-primary" data-toggle="modal" data-target="#editModalGambar<?= $item['id']; ?>">Edit</a>
                                        </td>
                                        </tr> -->

              <!-- Modal Edit unggulan Dua -->
                                            <!-- <div class="modal fade" id="editModalGambar<?= $item['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="editModalLabelGambar<?= $item['id']; ?>" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="editModalLabelGambar<?= $item['id']; ?>">Edit Gambar</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                              <?php echo form_open_multipart('Unggulan/edit_unggulan_dua');?>
                                                                <input type="hidden" name="id" value="<?= $item['id']; ?>">

                                                                <div class="form-group">
                                                                    <label for="kategori">kategori :</label>
                                                                    <input type="text" name="kategori" value="<?= $item['kategori']; ?>" required class="form-control">
                                                                </div>


                                                                     <div class="form-group">
                                                                        <label  for="gambar" class="form-label">Gambar Tiga</label>
                                                                        <input class="form-control" type="file" id="gambar" name="gambar" accept="image/png, image/jpeg, image/jpg, image/gif">
                                                                            <img src="<?= base_url('./gambar/' . $item['gambar']); ?>" alt="GambarTiga" width="150" class="img-thumbnail">
                                                                             <input type="hidden" name="gambar_lama" value="<?= $item['gambar']; ?>">
                                                                        </div>


                                                                <div class="form-group text-right">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                                    <input type="submit" class="btn btn-primary" value="Simpan">
                                                                </div>
                                                               <?php echo form_close(); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                            <?php  endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
 -->

    

 </div>
 </div>




