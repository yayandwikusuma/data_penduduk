
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800"><?= $title;?></h1>



                    
<nav class="navbar navbar-expand-lg navbar-light bg-light">

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
<!-- 
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
      <button type="button" class="btn btn-primary mb-3 mr-2" data-toggle="modal" data-target="#tambahModal1" >Tambah judul</button>
      </li>
     -->  
</nav>

    <!--  Unggulan satu -->

                     <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Unggulan Satu </h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered " id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                        <th style="width: 5%;">No.</th>
                                            <th style="width: 16%; text-align: center;" >Judul </th>
                                            <th style="width: 30%;text-align: center;">Deskripsi </th>
                                            <th style="text-align: center;">Gambar Satu </th>
                                             <th style="text-align: center;">Gambar Dua </th>
                                            <th style="width: 12%;text-align: center;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                          <?php
                                        foreach ($unggulan as $item) : ?>
                                        <tr>
                                        <th scope="row"><?= $item['id']; ?></th>
                                        <td><?= $item['judul'];  ?></td>
                                        <td style= "text-align: justify"><?= $item['deskripsi']; ?></td> 
                                        <td> <img src="<?= base_url('./gambar/' . $item['gambar_satu']); ?>" alt="Gambar Satu" width="100" style="display: block; margin: 0 auto; max-width: 200px; max-height: 1000px; width: auto; height: auto;"></td>
                                        <td> <img src="<?= base_url('./gambar/' . $item['gambar_dua']); ?>" alt="Gambar Satu" width="100" style="display: block; margin: 0 auto; max-width: 200px; max-height: 1000px; width: auto; height: auto;"></td>
                                        
                                        <td>
                                         <a href="#" class="badge badge-primary" data-toggle="modal" data-target="#editModalUnggulanSatu<?= $item['id']; ?>">Edit</a>
                                        </td>
                                        </tr>



                     <!-- Modal Edit unggulan Satu -->
                                            <div class="modal fade" id="editModalUnggulanSatu<?= $item['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="editModalLabelUnggulanSatu<?= $item['id']; ?>" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="editModalLabelUnggulanSatu<?= $item['id']; ?>">Edit Unggulan Satu</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <?php echo form_open_multipart('Unggulan/edit_unggulan_satu');?>
                                                                <input type="hidden" name="id" value="<?= $item['id']; ?>">

                                                                <div class="form-group">
                                                                    <label for="judul">Judul Satu :</label>
                                                                    <input type="text" name="judul" value="<?= $item['judul']; ?>" required class="form-control">
                                                                </div>

                                                                <div class="form-group">
                                                                    <label for="deskripsi">Deskripsi:</label>
                                                                    <textarea name="deskripsi" class="form-control" rows="4"><?= $item['deskripsi']; ?></textarea>
                                                                </div>

                                                                     <div class="form-group">
                                                                        <label  for="gambar_satu" class="form-label">Gambar Satu</label>
                                                                        <input class="form-control" type="file" id="gambar_satu" name="gambar_satu" accept="image/png, image/jpeg, image/jpg, image/gif">
                                                                            <img src="<?= base_url('./gambar/' . $item['gambar_satu']); ?>" alt="GambarSatu" width="150" class="img-thumbnail">
                                                                             <input type="hidden" name="gambar_dua_satu" value="<?= $item['gambar_satu']; ?>">
                                                                        </div>

                                                                         <div class="form-group">
                                                                        <label  for="gambar_dua" class="form-label">Gambar Dua</label>
                                                                        <input class="form-control" type="file" id="gambar_dua" name="gambar_dua" accept="image/png, image/jpeg, image/jpg, image/gif">
                                                                            <img src="<?= base_url('./gambar/' . $item['gambar_dua']); ?>" alt="GambarDua" width="150" class="img-thumbnail">
                                                                             <input type="hidden" name="gambar_dua_lama" value="<?= $item['gambar_dua']; ?>">
                                                                        </div>

                                                                <div class="form-group text-right">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                                    <input type="submit" class="btn btn-primary" value="Simpan">
                                                                </div>
                                                             <?php echo form_close(); ?>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                            <?php  endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                        <!--  Unggulan Satu -->

                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Unggulan Dua </h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered " id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                        <th style="width: 5%;">No.</th>
                                            <th style="width: 16%; text-align: center;" >Judul Satu</th>
                                            <th style="width: 30%;text-align: center;">Deskripsi Satu </th>
                                            <th style="text-align: center;">Gambar Tiga </th>
                                         
                                            <th style="width: 12%;text-align: center;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                          <?php
                                        foreach ($unggulan as $item) : ?>
                                        <tr>
                                        <th scope="row"><?= $item['id']; ?></th>
                                        <td><?= $item['judul_satu'];  ?></td>
                                        <td style= "text-align: justify"><?= $item['deskripsi_satu']; ?></td> 
                                        <td> <img src="<?= base_url('./gambar/' . $item['gambar_tiga']); ?>" alt="Gambar Satu" width="100" style="display: block; margin: 0 auto; max-width: 200px; max-height: 1000px; width: auto; height: auto;"></td>
                                        
                                        <td>
                                         <a href="#" class="badge badge-primary" data-toggle="modal" data-target="#editModalUnggulanDua<?= $item['id']; ?>">Edit</a>
                                        </td>
                                        </tr>

              <!-- Modal Edit unggulan Dua -->
                                            <div class="modal fade" id="editModalUnggulanDua<?= $item['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="editModalLabelUnggulanDua<?= $item['id']; ?>" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="editModalLabelUnggulanDua<?= $item['id']; ?>">Edit Unggulan Dua</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                              <?php echo form_open_multipart('Unggulan/edit_unggulan_dua');?>
                                                                <input type="hidden" name="id" value="<?= $item['id']; ?>">

                                                                <div class="form-group">
                                                                    <label for="judul_satu">Judul Satu :</label>
                                                                    <input type="text" name="judul_satu" value="<?= $item['judul_satu']; ?>" required class="form-control">
                                                                </div>

                                                                <div class="form-group">
                                                                    <label for="deskripsi_satu">Deskripsi:</label>
                                                                    <textarea name="deskripsi_satu" class="form-control" rows="4"><?= $item['deskripsi_satu']; ?></textarea>
                                                                </div>

                                                                     <div class="form-group">
                                                                        <label  for="gambar_TIGA" class="form-label">Gambar Tiga</label>
                                                                        <input class="form-control" type="file" id="gambar_tiga" name="gambar_tiga" accept="image/png, image/jpeg, image/jpg, image/gif">
                                                                            <img src="<?= base_url('./gambar/' . $item['gambar_tiga']); ?>" alt="GambarTiga" width="150" class="img-thumbnail">
                                                                             <input type="hidden" name="gambar_tiga_lama" value="<?= $item['gambar_tiga']; ?>">
                                                                        </div>


                                                                <div class="form-group text-right">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                                    <input type="submit" class="btn btn-primary" value="Simpan">
                                                                </div>
                                                               <?php echo form_close(); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                            <?php  endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


    

 </div>
 </div>




