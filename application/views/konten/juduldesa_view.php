
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800"><?= $title;?></h1>



                    
<nav class="navbar navbar-expand-lg navbar-light bg-light">

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
<!-- 
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
      <button type="button" class="btn btn-primary mb-3 mr-2" data-toggle="modal" data-target="#tambahModal1" >Tambah judul</button>
      </li>
     -->

 
    
</nav>




<table class="table">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Desa</th>
            <th>Gambar Icon</th>
            <th>Gambar Utama</th>
            <th>Slogan Satu</th>
            <th>Slogan Dua</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 1; 
        foreach ($judul_desa as $item) : ?>
            <tr>
                <th scope="row"><?= $i; ?></th>
                <td><?= $item['nama_desa'];  ?></td>
                <td class = "text-center">
                    <?php if (!empty($item['gambar_icon'])) : ?>
                        <img src="<?= base_url('./gambar/' . $item['gambar_icon']); ?>" alt="Gambar Icon" width="50">
                    <?php else : ?>
                        Tidak Ada Gambar
                    <?php endif; ?>
                </td>
                <td class = "text-center"> 
                    <?php if (!empty($item['gambar_utama'])) : ?>
                        <img src="<?= base_url('./gambar/' . $item['gambar_utama']); ?>" alt="Gambar Utama" width="100">
                    <?php else : ?>
                        Tidak Ada Gambar
                    <?php endif; ?>
                </td>
                <td><?= $item['slogan_satu']; ?></td>
                <td><?= $item['slogan_dua']; ?></td>
                <td>
                    <a href="#" class="badge badge-primary" data-toggle="modal" data-target="#editModal<?= $item['id']; ?>">Edit</a>
                    <!-- <a href="<?= base_url('JudulDesa/hapus_judul/' . $item['id']); ?>" class="badge badge-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Hapus</a> -->
                </td>
            </tr>
           
        </tbody>
</table>
        
                                <!-- Modal Edit Judul -->
                <div class="modal fade" id="editModal<?= $item['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="editModalLabel<?= $item['id']; ?>" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="editModalLabel<?= $item['id']; ?>">Edit Konten Header</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                            <?php echo form_open_multipart('JudulDesa/edit_judul');?>
                             
                                    <input type="hidden" name="id" value="<?= $item['id']; ?>">
                                    <div class="form-group">
                                        <label for="nama_desa">Nama Desa:</label>
                                        <input type="text" name="nama_desa" id="editNamaDesa<?= $item['id']; ?>" value="<?= $item['nama_desa']; ?>" required class="form-control">
                                    </div>

                                    
                                    <div class="form-group">
                                    <label  for="gambar_icon" class="form-label">Gambar Icon</label>
                                    <input class="form-control" type="file" id="gambar_icon" name="gambar_icon" accept="image/png, image/jpeg, image/jpg, image/gif">
                                 
                                            <img src="<?= base_url('./gambar/' . $item['gambar_icon']); ?>" alt="Gambar Icon" width="50" class="img-thumbnail">
                                     
                                    </div>

                                    

                                 <div class="form-group">
                                    <label  for="gambar_icon" class="form-label">Gambar Utama</label>
                                    <input class="form-control" type="file" id="gambar_utama" name="gambar_utama" accept="image/png, image/jpeg, image/jpg, image/gif">
                                
                                        <img src="<?= base_url('./gambar/' . $item['gambar_utama']); ?>" alt="Gambar Utama" width="150" class="img-thumbnail">
                                    
                                    </div>


                                <div class="form-group">
                                    <label for="slogan_satu">Slogan Satu:</label>
                                    <textarea name="slogan_satu" class="form-control"><?= $item['slogan_satu']; ?></textarea>
                                </div>

                                <div class="form-group">
                                    <label for="slogan_dua">Slogan Dua:</label>
                                    <textarea name="slogan_dua" class="form-control"><?= $item['slogan_dua']; ?></textarea>
                                </div>

                                    
                                    
                                    <div class="form-group text-right">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                        <input type="submit" class="btn btn-primary" value="Simpan">

                                       
                                    </div>
                                     <?php echo form_close(); ?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

               <?php $i++; endforeach; ?>
        </tbody>
        </table>

                                 <!-- Modal Tambah Judul -->            
                 <div class="modal fade" id="tambahModal1" tabindex="-1" role="dialog" aria-labelledby="tambahModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="tambahModalLabel">Tambah Judul header</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                            <form action="<?= base_url('JudulDesa/upload_gambar_icon'); ?>"  id="formUploadGambarIcon" method="post" enctype="multipart/form-data">

                                <div class="form-group">
                                    <label for="nama_desa">Nama:</label>
                                    <input type="text" name="nama_desa" required class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="gambar_icon">Gambar Icon:</label>
                                    <input type="file" name="gambar_icon" class ="form-control" size ="20">
                                </div>
<!-- 
                                 <div class="form-group">
                                    <label for="gambar_utama">Gambar Utama:</label>
                                    <input type="file" name="gambar_utama" class ="form-control" size ="50">
                                </div> -->

                      

                             <div class="form-group">
                                    <label for="slogan_satu">Slogan Satu:</label>
                                    <textarea name="slogan_satu" class="form-control"></textarea>
                                </div>

                                <div class="form-group">
                                    <label for="slogan_dua">Slogan Dua:</label>
                                    <textarea name="slogan_dua" class="form-control"></textarea>
                                </div>
                                <div class="form-group text-right">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                    <input type="submit" class="btn btn-primary" id="btnUploadGambarIcon" value="Simpan" >
                                </div>
                            </form>
                        </div>
                        </div>
                    </div>
                </div>



 </div>
 </div>

