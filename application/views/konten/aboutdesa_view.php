
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800"><?= $title;?></h1>



                    
<nav class="navbar navbar-expand-lg navbar-light bg-light">

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
<!-- 
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
      <button type="button" class="btn btn-primary mb-3 mr-2" data-toggle="modal" data-target="#tambahModal1" >Tambah judul</button>
      </li>
     -->  
</nav>

    <!--  Judul Utama -->

                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Judul Utama</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered " id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                        <th style="width: 5%;">No.</th>
                                            <th style="width: 32%; text-align: center;" >Judul Utama</th>
                                            <th style="text-align: center;">Deskripsi Utama</th>
                                            <th style="width: 12%;text-align: center;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                          <?php
                                        foreach ($tentang_desa as $item) : ?>
                                        <tr>
                                        <th scope="row"><?= $item['id']; ?></th>
                                        <td><?= $item['judul_utama'];  ?></td>
                                        <td style= "text-align: justify"><?= $item['deskripsi_utama']; ?></td>  
                                        <td>
                                            <a href="#" class="badge badge-primary" data-toggle="modal" data-target="#editModalJudulUtama<?= $item['id']; ?>">Edit</a>
                                        </td>
                                        </tr>

                                         <!-- Modal Edit Judul Utama -->
              <!-- Modal Edit Judul Utama -->
                            <div class="modal fade" id="editModalJudulUtama<?= $item['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="editModalLabelJudulUtama<?= $item['id']; ?>" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="editModalLabelJudulUtama<?= $item['id']; ?>">Edit Judul Utama</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="<?= base_url('TentangDesa/edit_judul_utama/' . $item['id']); ?>" method="post">
                                                <input type="hidden" name="id" value="<?= $item['id']; ?>">

                                                <div class="form-group">
                                                    <label for="judul_utama">Judul Utama:</label>
                                                    <input type="text" name="judul_utama" value="<?= $item['judul_utama']; ?>" required class="form-control">
                                                </div>

                                                <div class="form-group">
                                                    <label for="deskripsi_utama">Deskripsi:</label>
                                                    <textarea name="deskripsi_utama" class="form-control" rows="4"><?= $item['deskripsi_utama']; ?></textarea>
                                                </div>

                                                <div class="form-group text-right">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                    <input type="submit" class="btn btn-primary" value="Simpan">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

               <?php  endforeach; ?>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


    <!--  Judul satu -->
                        <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Card Satu</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                        <th style="width: 5%;">No.</th>
                                            <th style="width: 32%; text-align: center;" >Judul Box Satu</th>
                                            <th style="text-align: center;">Deskripsi </th>
                                            <th style="width: 12%;text-align: center;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                          <?php
                                        foreach ($tentang_desa as $item) : ?>
                                        <tr>
                                        <th scope="row"><?= $item['id']; ?></th>
                                        <td><?= $item['judul_satu'];  ?></td>
                                        <td style= "text-align: justify"><?= $item['deskripsi_satu']; ?></td>  
                                        <td>
                                            <a href="#" class="badge badge-primary" data-toggle="modal" data-target="#editModalSatu<?= $item['id']; ?>">Edit</a>
                                        </td>
                                        </tr>

                                                <!-- Modal Edit Card Satu -->
                                                <div class="modal fade" id="editModalSatu<?= $item['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="editModalLabelSatu<?= $item['id']; ?>" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="editModalLabelSatu<?= $item['id']; ?>">Edit Judul Card Satu</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="<?= base_url('TentangDesa/edit_card_satu/' . $item['id']); ?>" method="post">
                                                                    <input type="hidden" name="id" value="<?= $item['id']; ?>">

                                                                    <div class="form-group">
                                                                        <label for="judul_satu">Judul Card Satu:</label>
                                                                        <input type="text" name="judul_satu" value="<?= $item['judul_satu']; ?>" required class="form-control">
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label for="deskripsi_satu">Deskripsi:</label>
                                                                        <textarea name="deskripsi_satu" class="form-control" rows="4"><?= $item['deskripsi_satu']; ?></textarea>
                                                                    </div>

                                                                    <div class="form-group text-right">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                                        <input type="submit" class="btn btn-primary" value="Simpan">
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        <?php  endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                                                    <!--  Judul dua -->

                        <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Card Dua</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                        <th style="width: 5%;">No.</th>
                                            <th style="width: 32%; text-align: center;" >Judul Box Dua</th>
                                            <th style="text-align: center;">Deskripsi </th>
                                            <th style="width: 12%;text-align: center;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                          <?php 
                                        foreach ($tentang_desa as $item) : ?>
                                        <tr>
                                        <th scope="row"><?= $item['id']; ?></th>
                                        <td><?= $item['judul_dua'];  ?></td>
                                        <td style= "text-align: justify"><?= $item['deskripsi_dua']; ?></td>  
                                        <td>
                                            <a href="#" class="badge badge-primary" data-toggle="modal" data-target="#editModalDua<?= $item['id']; ?>">Edit</a>
                                        </td>
                                        </tr>

                                      <!-- Modal Edit Card Dua -->
                                                <div class="modal fade" id="editModalDua<?= $item['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="editModalLabelDua<?= $item['id']; ?>" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="editModalLabelDua<?= $item['id']; ?>">Edit Judul Card Dua</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="<?= base_url('TentangDesa/edit_card_dua/' . $item['id']); ?>" method="post">
                                                                    <input type="hidden" name="id" value="<?= $item['id']; ?>">

                                                                    <div class="form-group">
                                                                        <label for="judul_dua">Judul Card Dua:</label>
                                                                        <input type="text" name="judul_dua" value="<?= $item['judul_dua']; ?>" required class="form-control">
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label for="deskripsi_dua">Deskripsi:</label>
                                                                        <textarea name="deskripsi_dua" class="form-control" rows="4"><?= $item['deskripsi_dua']; ?></textarea>
                                                                    </div>

                                                                    <div class="form-group text-right">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                                        <input type="submit" class="btn btn-primary" value="Simpan">
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        <?php  endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                                                        <!--  Judul tiga -->
                      <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Card Tiga</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                        <th style="width: 5%;">No.</th>
                                            <th style="width: 32%; text-align: center;" >Judul Box Tiga</th>
                                            <th style="text-align: center;">Deskripsi </th>
                                            <th style="width: 12%;text-align: center;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                          <?php 
                                        foreach ($tentang_desa as $item) : ?>
                                        <tr>
                                        <th scope="row"><?= $item['id']; ?></th>
                                        <td><?= $item['judul_tiga'];  ?></td>
                                        <td><?= $item['deskripsi_tiga']; ?></td>  
                                        <td>
                                            <a href="#" class="badge badge-primary" data-toggle="modal" data-target="#editModalTiga<?= $item['id']; ?>">Edit</a>
                                        </td>
                                        </tr>

                                     <!-- Modal Edit Card Dua -->
                                                <div class="modal fade" id="editModalTiga<?= $item['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="editModalLabelTiga<?= $item['id']; ?>" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="editModalLabelTiga<?= $item['id']; ?>">Edit Judul Card Tiga</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="<?= base_url('TentangDesa/edit_card_tiga/' . $item['id']); ?>" method="post">
                                                                    <input type="hidden" name="id" value="<?= $item['id']; ?>">

                                                                    <div class="form-group">
                                                                        <label for="judul_tiga">Judul Card Tiga:</label>
                                                                        <input type="text" name="judul_tiga" value="<?= $item['judul_tiga']; ?>" required class="form-control">
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label for="deskripsi_tiga">Deskripsi:</label>
                                                                        <textarea name="deskripsi_tiga" class="form-control" rows="4"><?= $item['deskripsi_tiga']; ?></textarea>
                                                                    </div>

                                                                    <div class="form-group text-right">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                                        <input type="submit" class="btn btn-primary" value="Simpan">
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        <?php  endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--  Judul empat -->

                     <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Card Empat</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                        <th style="width: 5%;">No.</th>
                                            <th style="width: 32%; text-align: center;" >Judul Box Empat</th>
                                            <th style="text-align: center;">Deskripsi </th>
                                            <th style="width: 12%;text-align: center;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                          <?php 
                                        foreach ($tentang_desa as $item) : ?>
                                        <tr>
                                        <th scope="row"><?= $item['id']; ?></th>
                                        <td><?= $item['judul_empat'];  ?></td>
                                        <td style= "text-align: justify"><?= $item['deskripsi_empat']; ?></td>  
                                        <td>
                                            <a href="#" class="badge badge-primary" data-toggle="modal" data-target="#editModalEmpat<?= $item['id']; ?>">Edit</a>
                                        </td>
                                        </tr>

                                                                                    <!-- Modal Edit Card Empat -->
                                                <div class="modal fade" id="editModalEmpat<?= $item['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="editModalLabelEmpat<?= $item['id']; ?>" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="editModalLabelEmpat<?= $item['id']; ?>">Edit Judul Card Empat</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="<?= base_url('TentangDesa/edit_card_empat/' . $item['id']); ?>" method="post">
                                                                    <input type="hidden" name="id" value="<?= $item['id']; ?>">

                                                                    <div class="form-group">
                                                                        <label for="judul_empat">Judul Card Dua:</label>
                                                                        <input type="text" name="judul_empat" value="<?= $item['judul_empat']; ?>" required class="form-control">
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label for="deskripsi_empat">Deskripsi:</label>
                                                                        <textarea name="deskripsi_empat" class="form-control" rows="4"><?= $item['deskripsi_empat']; ?></textarea>
                                                                    </div>

                                                                    <div class="form-group text-right">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                                        <input type="submit" class="btn btn-primary" value="Simpan">
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        <?php  endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                   

 </div>
 </div>




