// assets/js/live_search.js
$(document).ready(function () {
  // Fungsi untuk melakukan pencarian live
  $("#liveSearch").on("input", function () {
    var query = $(this).val();

    // Menggunakan AJAX untuk mengirim data pencarian ke server
    $.ajax({
      url: baseUrl + "penduduk/live_search",
      method: "post",
      data: { query: query },
      dataType: "html",
      success: function (response) {
        // Tampilkan hasil pencarian di dalam tbody
        $("tbody").html(response);
      },
      error: function (xhr, ajaxOptions, thrownError) {
        alert("Error: " + xhr.status + " " + thrownError);
      },
    });
  });

  // Handle penghapusan dengan menggunakan class delete
  $("tbody").on("click", ".delete", function () {
    var id = $(this).data("id");
    // Lakukan penghapusan menggunakan AJAX
    $.ajax({
      url: baseUrl + "penduduk/hapus/" + id,
      method: "get",
      success: function (response) {
        // Tampilkan hasil pencarian setelah penghapusan
        $("tbody").html(response);
      },
      error: function (xhr, ajaxOptions, thrownError) {
        alert("Error: " + xhr.status + " " + thrownError);
      },
    });
  });
});
