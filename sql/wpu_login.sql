-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 31 Jan 2024 pada 03.23
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wpu_login`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `gambar`
--

CREATE TABLE `gambar` (
  `id` int(11) NOT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `nama_file` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `judul_desa`
--

CREATE TABLE `judul_desa` (
  `id` int(11) NOT NULL,
  `nama_desa` varchar(255) NOT NULL,
  `gambar_icon` varchar(255) DEFAULT NULL,
  `gambar_utama` varchar(255) DEFAULT NULL,
  `slogan_satu` text,
  `slogan_dua` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `judul_desa`
--

INSERT INTO `judul_desa` (`id`, `nama_desa`, `gambar_icon`, `gambar_utama`, `slogan_satu`, `slogan_dua`) VALUES
(1, 'Desa Patalan', 's1.png', 'Kotagede1.jpg', 'Selamat Datang Di Desa Prenggan', 'Desa wisata kotagede merupakan penghasil perak terbaik diyogyakarta');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kartu_keluarga`
--

CREATE TABLE `kartu_keluarga` (
  `id` int(11) NOT NULL,
  `no_kk` varchar(20) NOT NULL,
  `kepala_keluarga` varchar(255) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kartu_keluarga`
--

INSERT INTO `kartu_keluarga` (`id`, `no_kk`, `kepala_keluarga`, `alamat`) VALUES
(10, '3261251511365566111', 'yayan dwi kusuma', 'Prenggan'),
(11, '97887545646464662', 'Andika Pratama', 'pilahan wetan'),
(12, '326125151136534324', 'Sandiaga', 'krongahan'),
(13, '12555372122323221', 'sandrino sudiro', 'pamiharhjoo'),
(14, '3261251511365566322', 'suparjo sanidi', 'pongalan'),
(15, '436454754545322', 'sukirman kamijoro', 'sri harjo'),
(16, '343232222111', 'suryaaji ningrat', 'mbasen wetan'),
(17, '12123332121265', 'Sukirman kardi', 'plunyon wrwtean'),
(18, '4322343232132', 'Sri marjono', 'anismnarjanti wetan selokan'),
(19, '432324111124444', 'darmadi sulasteri', 'gibahan wetan'),
(20, '64328432424', 'kardiyono', 'rewrewrwr'),
(21, '323132131257767', 'Sulastri', 'kurnadi wetan'),
(22, '32315767768964543', 'sartono kemplu', 'eseesessse'),
(23, '3261251543533``', 'Kurniawan Adhi', 'jalan sorosutan wetanb'),
(24, '326124324321111', 'Kartoyono', 'Singularan wetan'),
(25, '5432164913123213', 'Sudarmaji salintri', 'kepuh wetan smaigaluh'),
(26, '57687788888882323', 'Tursinah kalimatu', 'kaliomambu atamsuois'),
(27, '34323421122647657', 'Tarmiji maliono', 'Patalan selatan'),
(28, '326125151136553434', 'Anggoro Putro', 'Seyegan kidul');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `nama_kategori` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id`, `nama_kategori`) VALUES
(1, 'Kerajinan Perak'),
(2, 'Wisata'),
(3, 'Kuliner'),
(4, 'Budaya');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelahiran`
--

CREATE TABLE `kelahiran` (
  `id` int(11) NOT NULL,
  `no_kk` varchar(20) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `kepala_keluarga` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kematian`
--

CREATE TABLE `kematian` (
  `id` int(11) NOT NULL,
  `no_ktp` varchar(225) DEFAULT NULL,
  `nama` varchar(25) NOT NULL,
  `tanggal_kematian` date DEFAULT NULL,
  `sebab_kematian` varchar(255) DEFAULT NULL,
  `alamat_pemakaman` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `penduduk`
--

CREATE TABLE `penduduk` (
  `id` int(11) NOT NULL,
  `no_ktp` varchar(20) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `alamat` text,
  `tanggal_lahir` date DEFAULT NULL,
  `umur` int(11) DEFAULT NULL,
  `jenis_kelamin` varchar(10) DEFAULT NULL,
  `pekerjaan` varchar(255) NOT NULL,
  `no_kk` varchar(20) DEFAULT NULL,
  `id_no_kk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penduduk`
--

INSERT INTO `penduduk` (`id`, `no_ktp`, `nama`, `alamat`, `tanggal_lahir`, `umur`, `jenis_kelamin`, `pekerjaan`, `no_kk`, `id_no_kk`) VALUES
(23, '321432432424200', 'sinta', 'ponggalan', '2000-12-12', 33, 'Perempuan', 'Ibu Rumah tangga', '12555372122323221', 13),
(24, '765555645654644', 'Sidik rahmadi ', 'essesese', '2004-02-09', 30, 'Laki-laki', 'karyawan swasta', '326125151136534324', 12),
(25, '99909654954433', 'surya pratama', 'jombrangan kidul', '2000-12-12', 30, 'Laki-laki', 'pegawai swasta', '326125151136556622', 18),
(26, '123456789', 'anindya wira', 'samigaluh selatan', '2001-12-08', 29, 'Perempuan', 'wirausaha', '97887545646464662', 10),
(33, '123', 'wdsdwad', 'wdwadawd', '2009-12-12', 32, 'Laki-laki', 'pegawai swasta', '3261251511365566123', 10),
(34, '1232452', 'yadi vbni', '123545', '2009-12-12', 23, 'Laki-laki', 'wirausaha', '3261251511365566123', 10),
(35, '1234', 'karmidi giono', 'dadeadd', '2004-12-12', 32, 'Laki-laki', 'wirausaha', '3261251511365566111', 10);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pindah_rumah`
--

CREATE TABLE `pindah_rumah` (
  `id` int(11) NOT NULL,
  `no_ktp` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `tanggal_pindah` date DEFAULT NULL,
  `alasan` varchar(255) DEFAULT NULL,
  `alamat_baru` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pindah_rumah`
--

INSERT INTO `pindah_rumah` (`id`, `no_ktp`, `nama`, `tanggal_pindah`, `alasan`, `alamat_baru`) VALUES
(1, '4543543243232355', 'supardi', '2024-01-02', 'rumah milik keluarga', 'sambirejo');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tentang_desa`
--

CREATE TABLE `tentang_desa` (
  `id` int(11) NOT NULL,
  `judul_utama` varchar(255) DEFAULT NULL,
  `deskripsi_utama` text,
  `judul_satu` varchar(255) DEFAULT NULL,
  `deskripsi_satu` text,
  `judul_dua` varchar(255) DEFAULT NULL,
  `deskripsi_dua` text,
  `judul_tiga` varchar(255) DEFAULT NULL,
  `deskripsi_tiga` text,
  `judul_empat` varchar(255) DEFAULT NULL,
  `deskripsi_empat` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tentang_desa`
--

INSERT INTO `tentang_desa` (`id`, `judul_utama`, `deskripsi_utama`, `judul_satu`, `deskripsi_satu`, `judul_dua`, `deskripsi_dua`, `judul_tiga`, `deskripsi_tiga`, `judul_empat`, `deskripsi_empat`) VALUES
(1, 'KOTAGEDE', 'Kotagede adalah sebuah kota lama yang terletak di Yogyakarta bagian selatan yang secara adminisratif terletak di kota Yogyakarta dan Kabupaten Bantul. Sebagai kota kuno bekas ibukota Kerajaan Mataram Islam yang ', 'MATA PENCAHARIAN', 'Mayoritas penduduk kotagede adalah pengrajin perak ada juga yang berdagang di pasar maupun kuliner.', 'WISATA', 'Banyak diantara kawasan dikotagede merupakan destinasi wisata seperti omah duwur ,sekar kedaton dan masih banyak yang lainnya.', 'DATA PENDUDUK', 'Untuk saat ini kotagede memiliki 1200kk yang terdiri dari berbagai macam daerah yang berada di kotagede', 'DESA MAJU', 'Disamping banyak wisata maupun kerajinan perak disana juga banyak terdapat peningkatan perekonomian warga karena banyak terdapat umkm .');

-- --------------------------------------------------------

--
-- Struktur dari tabel `unggulan`
--

CREATE TABLE `unggulan` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `deskripsi` text,
  `gambar_satu` varchar(255) DEFAULT NULL,
  `gambar_dua` varchar(255) DEFAULT NULL,
  `gambar_tiga` varchar(255) NOT NULL,
  `judul_satu` varchar(225) NOT NULL,
  `deskripsi_satu` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `unggulan`
--

INSERT INTO `unggulan` (`id`, `judul`, `deskripsi`, `gambar_satu`, `gambar_dua`, `gambar_tiga`, `judul_satu`, `deskripsi_satu`) VALUES
(1, 'MASJID MATARAM', 'Berdirinya Masjid Agung Kotagede sebagai masjid tertua di Yogyakarta tidak dapat dilepaskan dari keberadaan Kerajaan Mataram Islam atau Kesultanan Mataram. Wilayah Kotagede sebagai tempat berdirinyaKerajaan.Mataram Islam ini merupakan hadiah dari Raja Kasultanan Pajang (Sultan Hadiwijaya) kepada Ki Ageng Pamanahan atas keberhasilannya menangkap dan mengalahkan Aryo Penangsang. Setelah kerajaan Mataram Islam berdiri, Sunan Kalijaga memerintahkan pendirian masjid Agung Kotagede untuk memperluas syiar agama Islam. Hal ini tidak begitu saja menghapuskan kepercayaan yang sebelumnya telah dianut masyarakat yaitu tradisi kejawen, sebuah pandangan.', 'Kotagede.jpg', 'a.jpg', 'perak.jpg', 'Kerajinan Perak Kotagede', 'Meskipun popularitas kriya cukup tinggi, namun kriya menjadi salah satu subsektor ekonomi kreatif yang terdampak pandemi COVID-19. Mengutip dari Outlook Pariwisata dan Ekonomi Kreatif Indonesia 2020/2021, meskipun selama pandemi pertumbuhan subsektor kriya mengalami penurunan pendapatan sekitar -3,31%. Namun, subsektor kriya tetap berdiri tangguh menghadapi dampak pandemi dan berhasil bertahan secara perlahan. ');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `image` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `image`, `password`, `role_id`, `is_active`, `date_created`) VALUES
(4, 'yayan', 'yayan@gmail.com', 'default.jpg', '$2y$10$hlUVnkETNStWarLR2PHGcuX.xOf3NNjaPR3wAsdAkctPnlsqWIvO6', 1, 1, 1683036495),
(5, 'anggasap', 'anggasap@gmail.com', 'default.jpg', '$2y$10$0CiMvySFWzkQYyR/1pfuYeBkjsu5QQave.aAf6O5dFfqk0vseLQG.', 1, 1, 1684223320),
(6, 'nina', 'nina@gmail.com', 'default.jpg', '$2y$10$Vz1VzGean78/J9f4WyZW5uIv.EdgDI5YZ3e87.wx/IKktuQ294akW', 1, 1, 1687788681),
(7, 'adit', 'adit@gmail.com', 'default.jpg', '$2y$10$OU8Y8tTpkJvpKB35Zzhqp.S3fuySUwt9j2PhDfuIlWLeWTNppNE0K', 1, 1, 1692773788),
(8, 'yayan', 'yayandwi@gmail.com', 'default.jpg', '$2y$10$sYc1g9I7O7NPRfQispbp9u4zhKQBGXJ65sFHsfmeOJCqZM5J9NGUi', 1, 1, 1700019201),
(9, 'ydk', 'ydk@gmail.com', 'default.jpg', '$2y$10$qcpDbPvEwKS6ioDKvnKyF.k/IV3PP.Of8ZLNZCX7X/rTympWJL4SK', 1, 1, 1700450391),
(10, 'dwikusuma', 'yayan1@gmail.com', 'default.jpg', '$2y$10$SGhSDLxhPL86.lM6lBxRees/1SV3X.1M2TyU4wGFBAuISLYcgmZFq', 1, 1, 1700550180),
(11, 'soniya', 'soniya@gmail.com', 'default.jpg', '$2y$10$.RYRHJlYKHg/T97JkigMCeoxHEDK2HusXj94lf512O.FXEZ.4bITO', 2, 1, 1701833299);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2),
(4, 1, 3),
(5, 1, 4),
(6, 2, 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_menu`
--

CREATE TABLE `user_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_menu`
--

INSERT INTO `user_menu` (`id`, `menu`) VALUES
(1, 'Admin'),
(2, 'User'),
(3, 'Menu'),
(4, 'Konten');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'Administrator'),
(2, 'Member');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
(1, 1, 'Dashboard', 'admin', 'fas fa-fw fa-tachometer-alt', 1),
(2, 2, 'Dashboard', 'user/index', 'fas fa-fw fa-tachometer-alt', 1),
(3, 3, 'Menu Management', 'menu', 'fas fa-solid fa-folder', 1),
(4, 3, 'SubMenu Management', 'menu/submenu', 'fas fa-fw fa-folder-open', 1),
(6, 1, 'Role', 'admin/role', 'fas fa-fw fa-user-tie', 1),
(8, 2, 'Data Kartu Keluarga', 'KK', 'fas fa-fw fa-table', 1),
(9, 2, 'Data KTP', 'penduduk', 'fas fa-fw fa-solid fa-users', 1),
(10, 2, 'Data Kelahiran', 'kelahiran', 'fas fa-fw fa-child', 1),
(11, 2, 'Data Kematian', 'kematian', 'fas fa-fw fa-skull', 1),
(13, 2, 'Data Pindah', 'pindah', 'fas fa-fw fa-door-open', 1),
(14, 2, 'My Profil', 'profil', 'fas fa-fw fa-user', 1),
(15, 4, 'Judul', 'JudulDesa', 'fas fa-paint-brush', 1),
(16, 4, 'Tentang Desa', 'TentangDesa', 'fab fa-medapps', 1),
(17, 4, 'Unggulan', 'Unggulan', 'fab fa-fw  fab fa-readme', 1),
(18, 4, 'Upload Gallery', 'Gallery', 'far fa-file-image', 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `gambar`
--
ALTER TABLE `gambar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indeks untuk tabel `judul_desa`
--
ALTER TABLE `judul_desa`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kartu_keluarga`
--
ALTER TABLE `kartu_keluarga`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kelahiran`
--
ALTER TABLE `kelahiran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_no_kk` (`no_kk`);

--
-- Indeks untuk tabel `kematian`
--
ALTER TABLE `kematian`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_no_ktp` (`no_ktp`) USING BTREE;

--
-- Indeks untuk tabel `penduduk`
--
ALTER TABLE `penduduk`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_no_ktp` (`no_ktp`),
  ADD KEY `idx_no_kk` (`id_no_kk`) USING BTREE;

--
-- Indeks untuk tabel `pindah_rumah`
--
ALTER TABLE `pindah_rumah`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_no_ktp` (`no_ktp`);

--
-- Indeks untuk tabel `tentang_desa`
--
ALTER TABLE `tentang_desa`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `unggulan`
--
ALTER TABLE `unggulan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `gambar`
--
ALTER TABLE `gambar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `judul_desa`
--
ALTER TABLE `judul_desa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `kartu_keluarga`
--
ALTER TABLE `kartu_keluarga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `kelahiran`
--
ALTER TABLE `kelahiran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `kematian`
--
ALTER TABLE `kematian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `penduduk`
--
ALTER TABLE `penduduk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT untuk tabel `pindah_rumah`
--
ALTER TABLE `pindah_rumah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tentang_desa`
--
ALTER TABLE `tentang_desa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `unggulan`
--
ALTER TABLE `unggulan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `gambar`
--
ALTER TABLE `gambar`
  ADD CONSTRAINT `gambar_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kelahiran`
--
ALTER TABLE `kelahiran`
  ADD CONSTRAINT `kelahiran_ibfk_1` FOREIGN KEY (`no_kk`) REFERENCES `kartu_keluarga` (`no_kk`);

--
-- Ketidakleluasaan untuk tabel `kematian`
--
ALTER TABLE `kematian`
  ADD CONSTRAINT `kematian` FOREIGN KEY (`no_ktp`) REFERENCES `penduduk` (`no_ktp`);

--
-- Ketidakleluasaan untuk tabel `penduduk`
--
ALTER TABLE `penduduk`
  ADD CONSTRAINT `idx_kk` FOREIGN KEY (`id_no_kk`) REFERENCES `kartu_keluarga` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
